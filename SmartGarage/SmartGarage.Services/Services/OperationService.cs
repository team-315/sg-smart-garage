﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class OperationService : IOperationService
    {
        private readonly SmartGarageContext context;
        private readonly IDTOMapper mapper;

        public OperationService(SmartGarageContext context, IDTOMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(context));
        }

        private IQueryable<Operation> OperationQuery
        {
            get => context.Operations.Where(o => o.IsDeleted == null).Include(o => o.Visits).ThenInclude(ov => ov.Visit)
                .ThenInclude(v => v.Car).ThenInclude(c => c.User);
        }

        public async Task<OperationDTO> GetOperationAsync(int id)
        {
            var operation = await this.OperationQuery.FirstOrDefaultAsync(m => m.Id == id);

            if (operation == null)
            {
                throw new ResourceNotExistException("Operation does not exist");
            }

            var operationDTO = mapper.OperationToDTO(operation);

            return operationDTO;
        }

        public async Task<ICollection<OperationDTO>> GetAllOperationsAsync()
        {
            var operationDTOs = await OperationQuery.Select(m => mapper.OperationToDTO(m)).ToListAsync();

            return operationDTOs;
        }

        public async Task<ICollection<OperationDTO>> GetAllOperationsByNameAndPriceAsync(string name, int? price)
        {
            var operationDTOs = OperationQuery;

            if (name != null && name != "")
            {
                operationDTOs = operationDTOs.Where(o => o.Name.Contains(name));
            }

            if (price != 0 && price != null)
            {
                operationDTOs = operationDTOs.Where(o => o.PriceValue == price);
            }

            var result = await operationDTOs.Select(o => mapper.OperationToDTO(o)).ToListAsync();

            return result;
        }

        public async Task<OperationDTO> CreateAsync(OperationDTO model)
        {
            ValidateOperationNameNotExist(model.Name);
            ValidatePrice(model.PriceValue);

            var newOperation = mapper.OperationDTOToOperation(model);

            await this.context.Operations.AddAsync(newOperation);

            try
            {
                await this.context.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            return mapper.OperationToDTO(newOperation);
        }

        public async Task UpdateAsync(OperationDTO model)
        {
            var operationToUpdate = TryGetOperation(model.Id);

            ValidateOperationNameNotExist(model.Name,model.Id);
            ValidatePrice(model.PriceValue);

            operationToUpdate.PriceValue = model.PriceValue;

            this.context.Operations.Update(operationToUpdate);

            try
            {
                await this.context.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        public async Task DeleteAsync(int id)
        {
            var operation = TryGetOperation(id);
            operation.IsDeleted = DateTime.Now;

            this.context.Operations.Update(operation);
            await context.SaveChangesAsync();
        }

        public void ValidateOperationsExist(List<int> ids)
        {
            foreach (var operationId in ids)
            {
                bool operations = this.OperationQuery.Any(o => o.Id == operationId);

                if (!operations)
                {
                    throw new InvalidParameterException("Operation does not exist");
                }
            }
        }

        private void ValidatePrice(double priceValue)
        {
            if (priceValue < 0)
            {
                throw new InvalidParameterException("Price cannot be below 0");
            }
        }

        private void ValidateOperationNameNotExist(string name,int id)
        {
            bool operationNameExists = OperationQuery.Where(m=> m.Id != id).Any(o => o.Name == name);

            if (operationNameExists)
            {
                throw new ResourceAlreadyExistException($"Operation {name} already exists");
            }
        }

        private void ValidateOperationNameNotExist(string name)
        {
            bool operationNameExists = OperationQuery.Any(o => o.Name == name);

            if (operationNameExists)
            {
                throw new ResourceAlreadyExistException($"Operation {name} already exists");
            }
        }

        private Operation TryGetOperation(int id)
        {
            var operation = OperationQuery.Where(o => o.IsDeleted == null).FirstOrDefault(o => o.Id == id);

            if (operation == null)
            {
                throw new ResourceNotExistException("Operation does not exist");
            }

            return operation;
        }
    }
}
