﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class ManufacturerService : IManufacturerService
    {
        private readonly SmartGarageContext context;
        private readonly IDTOMapper mapper;

        public ManufacturerService(SmartGarageContext context, IDTOMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(context));
        }

        private IQueryable<Manufacturer> ManufacturerQuery
        {
            get => context.Manufacturers;
        }

        public async Task<ManufacturerDTO> GetManufacturerAsync(int id)
        {
            var manufacturer = await this.ManufacturerQuery.FirstOrDefaultAsync(m => m.Id == id);

            if (manufacturer == null)
            {
                throw new ResourceNotExistException("Manufacturer does not exist");
            }

            var manufacturerDTO = mapper.ManufacturerToDTO(manufacturer);

            return manufacturerDTO;
        }

        public async Task<ICollection<ManufacturerDTO>> GetAllManufacturersAsync()
        {
            var manufacturerDTOs = await ManufacturerQuery.Select(m => mapper.ManufacturerToDTO(m)).ToListAsync();
            if (manufacturerDTOs.Count == 0)
            {
                throw new ResourceNotExistException("There are no registered manufacturers");
            }
            return manufacturerDTOs;
        }

        public async Task<int> CreateIfNotExistAsync(string name)
        {
            var manufacturerExists = ManufacturerQuery.FirstOrDefault(m => m.Name == name);

            if (manufacturerExists == null)
            {
                string normalizedName = name.First().ToString().ToUpper() + name.Substring(1).ToLower();
                

                var newManufacturer = new Manufacturer();
                newManufacturer.Name = normalizedName;

                await context.Manufacturers.AddAsync(newManufacturer);

                try
                {
                    await context.SaveChangesAsync();
                }
                catch (ValidationException e)
                {
                    throw new InvalidParameterException(e.Message);
                }

                var manufacturer = await ManufacturerQuery.FirstOrDefaultAsync(m => m.Name == normalizedName);
                return manufacturer.Id;
            }
            else
            {
                return manufacturerExists.Id;
            }
            
        }

        private void ValidateManufacturerNameNotExist(string name)
        {
            bool manufacturerNameExists = context.Manufacturers.Any(m => m.Name == name);

            if (manufacturerNameExists)
            {
                throw new ResourceAlreadyExistException($"Manufacturer {name} already exists");
            }
        }
    }
}
