﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Data.Models.Constants;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class UserService : IUserService
    {
        private readonly SmartGarageContext context;
        private readonly IDTOMapper mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly JWT _jwt;

        public UserService(SmartGarageContext context, IDTOMapper mapper, UserManager<User> userManager,
                           RoleManager<Role> roleManager, IOptions<JWT> jwt)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(context));
            _roleManager = roleManager ?? throw new ArgumentNullException(nameof(context));
            _jwt = jwt.Value;
        }

        private IQueryable<User> UsersQuery
        {
            get => this.context.Users.Include(u => u.Cars).ThenInclude(c => c.Visits).Where(u => u.IsDeleted == null);
        }

        private IQueryable<User> AllUsersQuery
        {
            get => this.context.Users.Include(u => u.Cars).ThenInclude(c => c.Visits);
        }

        public async Task<UserDTO> GetUserAsync(int id)
        {
            var user = await this.UsersQuery.FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                throw new ResourceNotExistException("User does not exist");
            }

            var userDTO = mapper.UserToDTO(user);

            return userDTO;
        }

        public async Task<ICollection<UserDTO>> GetAllUsersAsync()
        {
            var userDTOs = await UsersQuery.Select(u => mapper.UserToDTO(u)).ToListAsync();

            return userDTOs;
        }

        public async Task<ICollection<UserDTO>> GetAllCustomersAsync()
        {
            var userDTOs = await UsersQuery.ToListAsync();

            List<User> result = await FilterCustomers(userDTOs);

            return mapper.UserToDTO(result);
        }

        public async Task<ICollection<UserDTOAdmin>> GetAllCustomersSortedByNameAsync()
        {
            var userDTOs = await UsersQuery.OrderBy(u => u.UserName).ToListAsync();

            List<User> result = await FilterCustomers(userDTOs);

            return result.Select(u => mapper.UserAdminToDTO(u)).ToList();
        }

        public async Task<ICollection<UserDTOAdmin>> GetAllCustomersSortedByVisitsAsync(HashSet<int> userIds)
        {
            var userDTOs = userIds.Select(id => UsersQuery.FirstOrDefault(x => x.Id == id)).ToList();

            List<User> result = await FilterCustomers(userDTOs);

            return result.Select(u => mapper.UserAdminToDTO(u)).ToList();
        }

        public async Task<ICollection<UserDTOAdmin>> GetAllCustomersIncludingDeletededFilteredAsync(CustomerFilterDTO model)
        {
            var users = AllUsersQuery;

            if (!string.IsNullOrEmpty(model.FirstName))
            {
                users = users.Where(u => (u.UserName+" "+u.LastName).Contains(model.FirstName));

            }

            if (!string.IsNullOrEmpty(model.Email))
            {
                users = users.Where(u => u.Email == model.Email);
            }

            if (!string.IsNullOrEmpty(model.Phone))
            {
                users = users.Where(u => u.PhoneNumber == model.Phone);
            }

            if (model?.CarId != 0)
            {
                users = users.Where(u => u.Cars.Any(c => c.Id == model.CarId));
            }

            if (model?.BeforeDate != null)
            {
                users = users.Where(u => u.Cars.Any(c => c.Visits.Any(v => v.Date > model.BeforeDate)));
            }

            if (model?.AfterDate != null)
            {
                users = users.Where(u => u.Cars.Any(c => c.Visits.Any(v => model.AfterDate > v.Date)));
            }

            List<User> result = await FilterCustomers(await users.ToListAsync());

            var res = result.Select(u => mapper.UserAdminToDTO(u)).ToList();

            return res;
        }

        public async Task<ICollection<UserDTO>> GetAllCustomersFilteredAsync(CustomerFilterDTO model)
        {
            var users = UsersQuery;

            if (!string.IsNullOrEmpty(model.FirstName))
            {
                users = users.Where(u => u.UserName == model.FirstName);
            }

            if (!string.IsNullOrEmpty(model.LastName))
            {
                users = users.Where(u => u.LastName == model.LastName);
            }

            if (!string.IsNullOrEmpty(model.Email))
            {
                users = users.Where(u => u.Email == model.Email);
            }

            if (!string.IsNullOrEmpty(model.Phone))
            {
                users = users.Where(u => u.PhoneNumber == model.Phone);
            }

            if (model?.CarId != 0)
            {
                users = users.Where(u => u.Cars.Any(c => c.Id == model.CarId));
            }

            if (model?.BeforeDate != null)
            {
                users = users.Where(u => u.Cars.Any(c => c.Visits.Any(v => v.Date > model.BeforeDate)));
            }

            if (model?.AfterDate != null)
            {
                users = users.Where(u => u.Cars.Any(c => c.Visits.Any(v => model.AfterDate > v.Date)));
            }

            List<User> result = await FilterCustomers(await users.ToListAsync());

            return mapper.UserToDTO(result);
        }

        public async Task<UserDTO> RegisterAsync(UserDTO model, string password)
        {
            ValidateUserNamesNotExist(model.UserName, model.LastName);
            ValidateUserEmail(model.Email);
            ValidateUserPhone(model.PhoneNumber);
            ValidatePassword(password);

            var newUser = new User
            {
                UserName = model.UserName,
                Email = model.Email,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                SecurityStamp = "7I5VNHIJHGZNOT3OPASFKUV4SDYBHFDS",
                ConcurrencyStamp = "11de0769-4567-1251-a963-1ac36aF33b19"
            };

            var passwordHasher = new PasswordHasher<User>();
            var pass = passwordHasher.HashPassword(newUser, password);
            newUser.PasswordHash = pass;

            await this.context.Users.AddAsync(newUser);
            await this.context.SaveChangesAsync();

            await _userManager.AddToRoleAsync(newUser, Authorization.Roles.Customer.ToString());

            return mapper.UserToDTO(newUser);
        }

        public async Task<AuthenticationModelDTO> GetTokenAsync(string email, string password)
        {
            var authenticationModelDTO = new AuthenticationModelDTO();
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                authenticationModelDTO.IsAuthenticated = false;
                authenticationModelDTO.Message = $"No Accounts Registered with {email}.";
                return authenticationModelDTO;
            }

            if (await _userManager.CheckPasswordAsync(user, password))
            {
                authenticationModelDTO.IsAuthenticated = true;
                JwtSecurityToken jwtSecurityToken = await CreateJwtToken(user);
                authenticationModelDTO.Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
                authenticationModelDTO.Email = user.Email;
                authenticationModelDTO.UserName = user.UserName;
                var rolesList = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
                authenticationModelDTO.Roles = rolesList.ToList();
                return authenticationModelDTO;
            }

            authenticationModelDTO.IsAuthenticated = false;
            authenticationModelDTO.Message = $"Incorrect Credentials for user {user.Email}.";
            return authenticationModelDTO;
        }

        private async Task<JwtSecurityToken> CreateJwtToken(User user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);
            var roleClaims = new List<Claim>();

            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim("roles", roles[i]));
            }

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("id", $"{user.Id}","int")
            }
            .Union(userClaims)
            .Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwt.Issuer,
                audience: _jwt.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_jwt.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }

        public async Task<string> AssignRoleAsync(AssignRoleDTO model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                throw new ResourceNotExistException($"No Accounts Registered with {model.Email}.");
            }

            var roleExists = Enum.GetNames(typeof(Authorization.Roles)).Any(x => x.ToLower() == model.Role.ToLower());
            if (roleExists)
            {
                var validRole = Enum.GetValues(typeof(Authorization.Roles)).Cast<Authorization.Roles>().FirstOrDefault(x => x.ToString().ToLower() == model.Role.ToLower());

                //Remove roles
                var roles = await _userManager.GetRolesAsync(user);
                await _userManager.RemoveFromRolesAsync(user, roles.ToArray());

                //Assign new role
                await _userManager.AddToRoleAsync(user, validRole.ToString());
                return $"Assigned {model.Role} to user {model.Email}.";
            }

            throw new ResourceNotExistException($"Role {model.Role} not found.");
        }

        public virtual async Task ChangePasswordAsync(string userEmail, string newPassword)
        {
            var userToUpdate = TryGetUser(userEmail);

            ValidatePassword(newPassword);

            try
            {
                var passwordHasher = new PasswordHasher<User>();
                var pass = passwordHasher.HashPassword(userToUpdate, newPassword);
                userToUpdate.PasswordHash = pass;

                this.context.Users.Update(userToUpdate);
                await this.context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        private void ValidatePassword(string newPassword)
        {
            if (newPassword.Length < 8)
            {
                throw new InvalidParameterException($"Password is too weak");
            }
        }

        public async Task UpdateAsync(UserDTO model)
        {
            var userToUpdate = TryGetUser(model.Id);

            ValidateUserNamesNotExist(model.UserName, model.LastName, model.Id);
            ValidateUserEmail(model.Email, model.Id);
            ValidateUserPhone(model.PhoneNumber, model.Id);

            userToUpdate.UserName = model.UserName;
            userToUpdate.LastName = model.LastName;
            userToUpdate.Email = model.Email;
            userToUpdate.PhoneNumber = model.PhoneNumber;

            try
            {
                await _userManager.UpdateAsync(userToUpdate);
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        public async Task DeleteAsync(int id)
        {
            var user = TryGetUser(id);

            user.IsDeleted = DateTime.Now;

            await _userManager.UpdateAsync(user);
        }


        public int TryGetUserId(string email)
        {
            var user = UsersQuery.FirstOrDefault(u => u.Email == email);

            if (user == null)
            {
                throw new ResourceNotExistException("User does not exist");
            }

            return user.Id;
        }

        public async Task<int> ValidateUserExist(int userId)
        {
            var userExist = await UsersQuery.FirstOrDefaultAsync(user => user.Id == userId);
            if (userExist == null)
            {
                throw new InvalidParameterException($"There is no user with id: {userId}");
            }
            else
            {
                return userExist.Id;
            }

        }

        public async Task<UserDTOAdmin> AdminGetUserAsync(int id)
        {
            var user = await context.Users.FirstOrDefaultAsync(user => user.Id == id);

            var userDTOAdmin = mapper.UserAdminToDTO(user);

            return userDTOAdmin;
        }

        public async Task<List<UserDTOAdmin>> AdminGetUsersAsync(int id)
        {

            var userDTOs = await context.Users.Where(u => u.Id != id).Select(u => mapper.UserAdminToDTO(u)).ToListAsync();

            return userDTOs;
        }

        public async Task UndoDeleteUserAsync(int id)
        {
            var user = TryGetDeletedUser(id);

            user.IsDeleted = null;

            await _userManager.UpdateAsync(user);
        }

        private async Task<List<User>> FilterCustomers(List<User> users)
        {
            var result = new List<User>();

            foreach (var item in users)
            {
                var rolesList = await _userManager.GetRolesAsync(item);

                if (rolesList != null && rolesList.Contains("Customer"))
                {
                    result.Add(item);
                }
            }

            return result;
        }

        private void ValidateUserPhone(string phone)
        {
            bool phoneExists = UsersQuery.Any(u => u.PhoneNumber == phone);

            if (phoneExists)
            {
                throw new ResourceAlreadyExistException($"User phone {phone} already exists");
            }

            if (phone.Length != 10 || !phone.StartsWith('0'))
            {
                throw new InvalidParameterException($"Phone number {phone} is not in the valid format");
            }
        }

        private void ValidateUserPhone(string phone, int id)
        {
            bool phoneExists = UsersQuery.Where(u => u.Id != id).Any(u => u.PhoneNumber == phone);

            if (phoneExists)
            {
                throw new ResourceAlreadyExistException($"User phone {phone} already exists");
            }

            if (phone.Length != 10 && !phone.StartsWith('0'))
            {
                throw new InvalidParameterException($"Phone number {phone} is not in the valid format");
            }
        }

        private void ValidateUserEmail(string email)
        {
            bool emailExists = UsersQuery.Any(u => u.Email == email);

            if (emailExists)
            {
                throw new ResourceAlreadyExistException($"User Email {email} already exists");
            }

            if (!email.Contains("@") || !email.Contains("."))
            {
                throw new InvalidParameterException($"Email {email} is not in the valid format");
            }
        }

        private void ValidateUserEmail(string email, int id)
        {
            bool emailExists = UsersQuery.Where(u => u.Id != id).Any(u => u.Email == email);

            if (emailExists)
            {
                throw new ResourceAlreadyExistException($"User Email {email} already exists");
            }

            if (!email.Contains("@") || !email.Contains("."))
            {
                throw new InvalidParameterException($"Email {email} is not in the valid format");
            }
        }
        private void ValidateUserNamesNotExist(string firstName, string lastName)
        {
            bool userWithNamesExists = UsersQuery.Where(u => u.UserName == firstName).Any(u => u.LastName == lastName);

            if (userWithNamesExists)
            {
                throw new ResourceAlreadyExistException($"User names {firstName} {lastName} already exists together");
            }

            if (firstName.Length < 2 || firstName.Length > 20)
            {
                throw new InvalidParameterException($"Names should be between 2 and 20 symbols.");
            }

            if (lastName.Length < 2 || lastName.Length > 20)
            {
                throw new InvalidParameterException($"Names should be between 2 and 20 symbols.");
            }
        }

        private void ValidateUserNamesNotExist(string firstName, string lastName, int id)
        {
            bool userWithNamesExists = UsersQuery.Where(u => u.Id != id).Where(u => u.LastName == lastName).Any(u => u.UserName == firstName);

            if (userWithNamesExists)
            {
                throw new ResourceAlreadyExistException($"User names {firstName} {lastName} already exists together");
            }

            if (firstName.Length < 2 || firstName.Length > 20)
            {
                throw new InvalidParameterException($"Names should be between 2 and 20 symbols.");
            }

            if (lastName.Length < 2 || lastName.Length > 20)
            {
                throw new InvalidParameterException($"Names should be between 2 and 20 symbols.");
            }
        }

        private User TryGetUser(int id)
        {
            var user = UsersQuery.FirstOrDefault(u => u.Id == id);

            if (user == null)
            {
                throw new ResourceNotExistException("User does not exist");
            }

            return user;
        }

        private User TryGetUser(string email)
        {
            var user = UsersQuery.FirstOrDefault(u => u.Email == email);

            if (user == null)
            {
                throw new ResourceNotExistException("User does not exist");
            }

            return user;
        }

        private User TryGetDeletedUser(int id)
        {
            var user = context.Users.Where(user => user.IsDeleted != null)
                                      .FirstOrDefault(user => user.Id == id);

            if (user == null)
            {
                throw new ResourceNotExistException("There are no deleted users.");
            }
            else
            {
                return user;
            }
        }
    }
}
