﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class CarModelService : ICarModelService
    {
        private readonly SmartGarageContext context;
        private readonly IDTOMapper mapper;
        public CarModelService(SmartGarageContext context, IDTOMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(context));
        }
        private IQueryable<CarModel> CarModelsQuery
        {
            get => context.CarModels
                .Include(x => x.Manufacturer);
        }
        public async Task<CarModelDTO> GetCarModelAsync(int id)
        {
            var carModel = await this.CarModelsQuery.FirstOrDefaultAsync(model => model.Id == id);

            if (carModel == null)
            {
                throw new ResourceNotExistException("Car model does not exist");
            }

            var carModelDTO = mapper.CarModelToDTO(carModel);

            return carModelDTO;
        }
        public async Task<ICollection<CarModelDTO>> GetAllCarModelsAsync()
        {
            var carModelDTOs = await CarModelsQuery.Select(model => mapper.CarModelToDTO(model)).ToListAsync();
            if (carModelDTOs.Count == 0)
            {
                throw new ResourceNotExistException("There are no registered car models");
            }
            return carModelDTOs;
        }
        public async Task<int> CreateCarModelIfNotExistAsync(string name, int manufacturerId)
        {
            var carModelExists = await CarModelsQuery.FirstOrDefaultAsync(cm => cm.Name == name);
            
            if (carModelExists == null)
            {
                var newCarModel = new CarModel();
                newCarModel.ManufacturerId = manufacturerId;
                newCarModel.Name = name;

                await context.CarModels.AddAsync(newCarModel);

                try
                {
                    await context.SaveChangesAsync();
                }
                catch (ValidationException e)
                {
                    throw new InvalidParameterException(e.Message);
                }
                var carModel = await CarModelsQuery.FirstOrDefaultAsync(m => m.Name == name);
                return carModel.Id;
            }
            else
            {
                return carModelExists.Id;
            }
            
        }
        public void ValidateCarModelExist(string name)
        {
            var carModelNameExists = context.CarModels.Any(cm => cm.Name == name);

            if (carModelNameExists)
            {
                throw new ResourceAlreadyExistException($"Car model {name} already exists");
            }
        }
    }
}
