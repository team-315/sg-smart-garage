﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class VisitService : IVisitService
    {
        private readonly SmartGarageContext context;
        private readonly IDTOMapper mapper;

        public VisitService(SmartGarageContext context, IDTOMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(context));
        }

        private IQueryable<Visit> VisitsQuery
        {
            get => this.context.Visits.Include(v => v.Visits).ThenInclude(ov => ov.Operation)
                .Include(v => v.Car).ThenInclude(c => c.User);
        }

        public async Task<VisitDTO> GetVisitAsync(int id)
        {
            var visit = await this.VisitsQuery.FirstOrDefaultAsync(m => m.Id == id);

            if (visit == null)
            {
                throw new ResourceNotExistException("Visit does not exist");
            }

            var visitDTO = mapper.VisitToDTO(visit);

            return visitDTO;
        }

        public async Task<ICollection<VisitDTO>> GetAllVisitsAsync()
        {
            var visitsDTOs = await VisitsQuery.Select(v => mapper.VisitToDTO(v)).ToListAsync();

            return visitsDTOs;
        }

        public async Task<HashSet<int>> GetAllUserIdsOrderedByVisitDate()
        {
            var visitDTOs = await VisitsQuery.OrderByDescending(v => v.Date)
                .ToListAsync();

            var customerIds = new HashSet<int>();

            foreach (var visit in visitDTOs)
            {
                customerIds.Add(visit.Car.UserId);
            }

            return customerIds;
        }

        public async Task<List<VisitDTO>> GetAllOperationsFiltered(int userId, OperationsFilterDTO model = null)
        {
            var visits = VisitsQuery.Where(v => v.Car.UserId == userId);

            if (model?.VehicleId != null)
            {
                visits = visits.Where(v => v.CarId == model.VehicleId);
            }

            if (model?.BeforeDate != null)
            {
                visits = visits.Where(v => v.Date < model.BeforeDate);
            }

            if (model?.AfterDate != null)
            {
                visits = visits.Where(v => v.Date > model.AfterDate);
            }

            var operationDTOs = await visits.Select(x => mapper.VisitToDTO(x)).ToListAsync();

            return operationDTOs;
        }

        public async Task<int> CreateAsync(int carId, List<int> ids)
        {
            var visitToCreate = new Visit();
            visitToCreate.CarId = carId;
            visitToCreate.Date = DateTime.Now;

            this.context.Visits.Add(visitToCreate);

            try
            {
                await context.SaveChangesAsync();
            }
            catch (InvalidParameterException e)
            {
                throw new InvalidParameterException(e.Message);
            }

            var visitId = await GetVisitIdAsync(carId, visitToCreate.Date);

            await AddOperationsToVisitAsync(visitId, ids);

            return visitId;
        }

        private async Task<int> GetVisitIdAsync(int carId, DateTime createdVisitDate)
        {
            var visit = await this.VisitsQuery.Where(v => v.CarId == carId).FirstOrDefaultAsync(m => m.Date == createdVisitDate);

            return visit.Id;
        }

        private async Task AddOperationsToVisitAsync(int createdVisitId, List<int> ids)
        {
            var listOV = new List<OperationVisit>();

            foreach (int id in ids)
            {
                var OV = new OperationVisit { VisitId = createdVisitId, OperationId = id };
                listOV.Add(OV);
            }

            try
            {
                context.AddRange(listOV);
                await context.SaveChangesAsync();
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException(e.Message);
            }
        }

        public async Task UpdateCarForVisitAsync(int id, int carId)
        {
            var visitToUpdate = TryGetVisit(id);

            visitToUpdate.CarId = carId;

            context.Visits.Update(visitToUpdate);

            try
            {
                await context.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        private Visit TryGetVisit(int id)
        {
            var visit = VisitsQuery.FirstOrDefault(o => o.Id == id);

            if (visit == null)
            {
                throw new ResourceNotExistException("Visit does not exist");
            }

            return visit;
        }
    }
}
