﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class CarService : ICarService
    {
        private readonly SmartGarageContext context;
        private readonly IDTOMapper mapper;
        public CarService(SmartGarageContext context, IDTOMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(context));
        }
        private IQueryable<Car> CarsQuery
        {
            get => context.Cars.Include(u => u.User)
                               .Include(c => c.CarModel)
                               .ThenInclude(m => m.Manufacturer);

        }
        public async Task<ICollection<CarDTO>> GetAllCarsAsync()
        {
            var carDTOs = await CarsQuery.Select(model => mapper.CarToDTO(model)).ToListAsync();
            if (carDTOs.Count == 0)
            {
                throw new ResourceNotExistException("There are no registered cars");
            }
            return carDTOs;
        }

        public async Task<CarDTO> GetCarAsync(int id)
        {
            var car = await this.CarsQuery.FirstOrDefaultAsync(m => m.Id == id);

            if (car == null)
            {
                throw new ResourceNotExistException("Car does not exist");
            }

            var carDTO = mapper.CarToDTO(car);

            return carDTO;
        }
        public async Task<ICollection<CarDTO>> GetAllCarsForUser(int id)
        {
            var cars = CarsQuery;

            if (id != 0)
            {
                cars = cars.Where(c => c.UserId == id);
            }

            var carDTOs = await cars.Select(c => mapper.CarToDTO(c)).ToListAsync();
                                         
            return carDTOs;
        }

        public async Task<CarDTO> CreateCarAsync(CarDTO carDTO)
        {
            ValidateCarVIN(carDTO.VIN);
            ValidateCarRegistrationPlateNotExist(carDTO.RegistrationPlate);

            var newCar = mapper.CarDTOToCar(carDTO);

            try
            {
                await context.Cars.AddAsync(newCar);
                await context.SaveChangesAsync();
            }
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
            return carDTO;
        }

        public async Task UpdateCarAsync(CarDTO carDTO)
        {
            Car car = TryGetCar(carDTO.Id);

            if (car.VIN != carDTO.VIN)
            {
                ValidateCarVIN(carDTO.VIN);
            }

            if (car.RegistrationPlate != carDTO.RegistrationPlate)
            {
                ValidateCarRegistrationPlateNotExist(carDTO.RegistrationPlate);
            }

            carDTO.UserId = car.UserId;
            
            car = mapper.CarDTOToCar(carDTO);

            context.Cars.Update(car);

            try
            {
                await context.SaveChangesAsync();
            }
            
            catch (ValidationException e)
            {
                throw new InvalidParameterException(e.Message);
            }
        }

        public void ValidateCarExists(int carId)
        {
            bool carExists = this.CarsQuery.Where(c => c.IsDeleted == null).Any(c => c.Id == carId);

            if (!carExists)
            {
                throw new InvalidParameterException("Car does not exist");
            }
        }

        private Car TryGetCar(int id)
        {
            var car = context.Cars.AsNoTracking().FirstOrDefault(i => i.Id == id);

            if (car == null)
            {
                throw new ResourceNotExistException("Car does not exist");
            }

            return car;
        }

        private void ValidateCarVIN(string vin)
        {
            var vinExists = context.Cars.Any(c => c.VIN == vin);

            if (vinExists)
            {
                throw new ResourceAlreadyExistException($"Car with VIN: {vin} already exists");
            }

            if (vin.Length != 17)
            {
                throw new InvalidParameterException($"Car VIN must be exactly 17 characters");
            }
        }

        private void ValidateCarRegistrationPlateNotExist(string plate)
        {
            var plateExists = context.Cars.Any(c => c.RegistrationPlate == plate);

            if (plateExists)
            {
                throw new ResourceAlreadyExistException($"Car with registration plate: {plate} already exists");
            }

            if (plate.Length != 8 || !Char.IsLetter(plate[0]) || !Char.IsLetter(plate[7]))
            {
                throw new InvalidParameterException("Not a valid bulgarian license plate");
            }
        }
    }
}
