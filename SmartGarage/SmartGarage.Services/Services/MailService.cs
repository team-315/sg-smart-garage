﻿using MailKit.Net.Smtp;
using MailKit.Security;
using SmartGarage.Data.Settings;
using Microsoft.Extensions.Options;
using MimeKit;
using System.IO;
using SmartGarage.Services.Contracts;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;
        public MailService(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        public async Task SendWelcomeEmailAsync(string userName, string toEmail)
        {
            string FilePath = Directory.GetCurrentDirectory() + "\\Templates\\WelcomeTemplate.html";

            StreamReader str = new StreamReader(FilePath);
            string MailText = str.ReadToEnd();
            str.Close();

            MailText = MailText.Replace("[username]", userName).Replace("[email]", toEmail);
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse("mrdonu6@gmail.com");
            email.To.Add(MailboxAddress.Parse("rbozhkov77@gmail.com"));
            email.Subject = $"Welcome {userName}";

            var builder = new BodyBuilder();
            builder.HtmlBody = MailText;
            email.Body = builder.ToMessageBody();

            using var smtp = new SmtpClient();
            smtp.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
            smtp.Authenticate("mrdonu6@gmail.com", "rosenpresen");

            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }

        public async Task SendForgotPasswordEmail(string userName, string url)
        {
            //string FilePath = Directory.GetCurrentDirectory() + "\\Templates\\WelcomeTemplate.html";

            //StreamReader str = new StreamReader(,);
            //string MailText = str.ReadToEnd();
            //str.Close();

            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse("mrdonu6@gmail.com");
            email.To.Add(MailboxAddress.Parse("rbozhkov77@gmail.com"));
            email.Subject = $"Reset password {userName}";

            var builder = new BodyBuilder();
            builder.HtmlBody = url;
            email.Body = builder.ToMessageBody();

            using var smtp = new SmtpClient();
            smtp.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
            smtp.Authenticate("mrdonu6@gmail.com", "rosenpresen");

            await smtp.SendAsync(email);
            smtp.Disconnect(true);
        }
    }
}
