﻿using Newtonsoft.Json;
using RestSharp;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.Services
{
    public class CurrencyConverterService : ICurrencyConverterService
    {
        public CurrencySettings GetCurrency(string desiredCurrency)
        {
            string url = "http://data.fixer.io/api/latest?access_key=471824e1273e988fb7c2654b06fa9b94&symbols="+$"{desiredCurrency}";
            
            var client = new RestClient(url);
            
            client.Timeout = -1;
            
            var request = new RestRequest(Method.GET);
            
            IRestResponse response = client.Execute(request);
            
            var stringResponse = response.Content.ToString();

            var myResult = JsonConvert.DeserializeObject<CurrencySettings>(stringResponse);

            return myResult;
        }
    }
}
