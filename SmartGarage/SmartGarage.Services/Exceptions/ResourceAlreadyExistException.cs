﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.Exceptions
{
    public class ResourceAlreadyExistException : Exception
    {
        public ResourceAlreadyExistException(string message)
            : base(message)
        {
        }
    }
}
