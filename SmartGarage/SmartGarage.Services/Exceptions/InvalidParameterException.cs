﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.Exceptions
{
    public class InvalidParameterException : Exception
    {
        public InvalidParameterException(string message):base (message)
        {

        }
    }
}
