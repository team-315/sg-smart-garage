﻿using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SmartGarage.Services.Mapper
{
    public class DTOMapper : IDTOMapper
    {

        public ManufacturerDTO ManufacturerToDTO(Manufacturer model)
        {
            var manufacturerDTO = new ManufacturerDTO();

            manufacturerDTO.Id = model.Id;
            manufacturerDTO.Name = model.Name;

            return manufacturerDTO;
        }

        public ICollection<UserDTO> UserToDTO(List<User> models)
        {
            return models.Select(u => UserToDTO(u)).ToList();
        }

        public Manufacturer ManufacturerDTOToManufacturer(ManufacturerDTO model)
        {
            var manufacturer = new Manufacturer();

            manufacturer.Id = model.Id;
            manufacturer.Name = model.Name;

            return manufacturer;
        }
        public CarModelDTO CarModelToDTO(CarModel model)
        {
            CarModelDTO carModelDTO = new CarModelDTO();

            var manufacturerDTO = ManufacturerToDTO(model.Manufacturer);

            carModelDTO.Id = model.Id;
            carModelDTO.Name = model.Name;
            carModelDTO.ManufacturerId = model.ManufacturerId;
            carModelDTO.Manufacturer = manufacturerDTO;
            carModelDTO.Manufacturer.Name = manufacturerDTO.Name;

            return carModelDTO;
        }
        public CarModel CarModelDTOToCarModel(CarModelDTO carModelDTO)
        {
            var carModel = new CarModel();
 
            carModel.Id = carModelDTO.Id;
            carModel.Name = carModelDTO.Name;
            carModel.ManufacturerId = carModelDTO.ManufacturerId;
           
            return carModel;
        }

        public OperationDTO OperationToDTO(Operation model)
        {
            var operationDTO = new OperationDTO();

            operationDTO.Id = model.Id;
            operationDTO.Name = model.Name;
            operationDTO.PriceValue = model.PriceValue;
            operationDTO.IsDeleted = model.IsDeleted;

            return operationDTO;
        }

        public Operation OperationDTOToOperation(OperationDTO model)
        {
            var operation = new Operation();

            operation.Id = model.Id;
            operation.Name = model.Name;
            operation.PriceValue = model.PriceValue;
            operation.IsDeleted = model.IsDeleted;

            return operation;
        }

        public VisitDTO VisitToDTO(Visit model)
        {
            var visitDTO = new VisitDTO();

            visitDTO.Id = model.Id;
            visitDTO.Date = (model.Date.ToString("dd/MM/yyyy HH:mm"));
            visitDTO.CarId = model.CarId;
            visitDTO.Operations = model.Visits.Select(ov => OperationToDTO(ov.Operation)).ToList();

            
            visitDTO.TotalPrice = model.Visits.Sum(ov => ov.Operation.PriceValue);

            return visitDTO;
        }
        public CarDTO CarToDTO(Car model)
        {
            var carDTO = new CarDTO();
            var carModelDTO = CarModelToDTO(model.CarModel);
            var userDTO = UserToDTO(model.User);

            carDTO.Id = model.Id;
            carDTO.CarModelId = model.CarModelId;
            carDTO.CarModel = carModelDTO;
            carDTO.VIN = model.VIN;
            carDTO.RegistrationPlate = model.RegistrationPlate;
            carDTO.UserId = model.UserId;
            carDTO.User = userDTO;

            return carDTO;
        }
        public Car CarDTOToCar(CarDTO carDTO)
        {
            var car = new Car();
            car.Id = carDTO.Id;
            car.CarModelId = carDTO.CarModelId;
            car.VIN = carDTO.VIN;
            car.UserId = carDTO.UserId;
            car.RegistrationPlate = carDTO.RegistrationPlate;

            return car;
        }
        public UserDTO UserToDTO(User model)
        {
            var userDTO = new UserDTO();
            userDTO.Id = model.Id;
            userDTO.UserName = model.UserName;
            userDTO.LastName = model.LastName;

            userDTO.Email = model.Email;
            userDTO.PhoneNumber = model.PhoneNumber;

            return userDTO;
        }

        public User UserDTOToUser(UserDTO model)
        {
            var user = new User();

            user.UserName = model.UserName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.PhoneNumber = model.PhoneNumber;

            return user;
        }

        public UserDTOAdmin UserAdminToDTO(User model)
        {
            var userDTO = new UserDTOAdmin();
            userDTO.Id = model.Id;
            userDTO.UserName = model.UserName;
            userDTO.LastName = model.LastName;
            
            userDTO.Email = model.Email;
            userDTO.PhoneNumber = model.PhoneNumber;
            userDTO.IsDeleted = model.IsDeleted;

            return userDTO;
        }
    }
}
