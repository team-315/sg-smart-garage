﻿using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using System.Collections.Generic;

namespace SmartGarage.Services.Mapper
{
    public interface IDTOMapper
    {
        Car CarDTOToCar(CarDTO carDTO);
        CarModel CarModelDTOToCarModel(CarModelDTO carModelDTO);
        CarModelDTO CarModelToDTO(CarModel model);
        CarDTO CarToDTO(Car model);
        Manufacturer ManufacturerDTOToManufacturer(ManufacturerDTO model);
        ManufacturerDTO ManufacturerToDTO(Manufacturer model);
        Operation OperationDTOToOperation(OperationDTO model);
        OperationDTO OperationToDTO(Operation model);
        User UserDTOToUser(UserDTO model);
        UserDTO UserToDTO(User model);
        VisitDTO VisitToDTO(Visit model);
        ICollection<UserDTO> UserToDTO(List<User> models);
        UserDTOAdmin UserAdminToDTO(User model);
    }
}