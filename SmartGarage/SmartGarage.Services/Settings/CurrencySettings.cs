﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.Settings
{
    public class CurrencySettings
    {
        public bool Success { get; set; }
        public DateTime Date { get; set; }
        public string Base { get; set; }
        public Dictionary<string, double> Rates { get; set; } = new Dictionary<string, double>();
        
    }
}
