﻿using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTOs
{
    public class VisitDTO
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public int CarId { get; set; }
        public double TotalPrice { get; set; }
        public List<OperationDTO> Operations { get; set; }
    }
}
