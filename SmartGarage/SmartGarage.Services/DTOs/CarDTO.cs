﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SmartGarage.Services.DTOs
{
    public class CarDTO
    {
        public int Id { get; set; }
        public string RegistrationPlate { get; set; } 
        public string VIN { get; set; } 
        public int CarModelId { get; set; }
        [JsonIgnore]
        public CarModelDTO CarModel { get; set; }
        public int UserId { get; set; }
        [JsonIgnore]
        public UserDTO User { get; set; }
        [JsonIgnore]
        public DateTime? IsDeleted { get; set; }
    }
}
