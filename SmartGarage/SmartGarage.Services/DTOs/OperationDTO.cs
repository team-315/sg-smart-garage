﻿using SmartGarage.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTOs
{
    public class OperationDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PriceValue { get; set; }
        public string PriceCurrency { get; set; }
        public DateTime? IsDeleted { get; set; }
    }
}
