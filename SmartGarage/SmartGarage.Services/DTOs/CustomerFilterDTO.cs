﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTOs
{
    public class CustomerFilterDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int CarId { get; set; }
        public DateTime? BeforeDate { get; set; }
        public DateTime? AfterDate { get; set; }
    }
}
