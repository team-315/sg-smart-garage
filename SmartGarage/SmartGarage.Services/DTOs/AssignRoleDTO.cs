﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTOs
{
    public class AssignRoleDTO
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
