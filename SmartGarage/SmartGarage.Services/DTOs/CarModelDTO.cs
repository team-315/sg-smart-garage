﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTOs
{
    public class CarModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        public ManufacturerDTO Manufacturer { get; set; }
    }
}
