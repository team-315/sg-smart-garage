﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.DTOs
{
    public class UserDTOAdmin
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? IsDeleted { get; set; }
    }
}
