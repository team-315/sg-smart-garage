﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface ICarModelService
    {
        Task<CarModelDTO> GetCarModelAsync(int id);
        Task<ICollection<CarModelDTO>> GetAllCarModelsAsync();
        Task<int> CreateCarModelIfNotExistAsync(string name, int manufacturerId);
        
    }
}
