﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> GetUserAsync(int id);
        Task<ICollection<UserDTO>> GetAllUsersAsync();
        Task<ICollection<UserDTO>> GetAllCustomersAsync();
        Task<ICollection<UserDTOAdmin>> GetAllCustomersSortedByNameAsync();
        Task<ICollection<UserDTOAdmin>> GetAllCustomersSortedByVisitsAsync(HashSet<int> userIds);
        Task<ICollection<UserDTO>> GetAllCustomersFilteredAsync(CustomerFilterDTO model);
        Task<ICollection<UserDTOAdmin>> GetAllCustomersIncludingDeletededFilteredAsync(CustomerFilterDTO model);
        Task<UserDTO> RegisterAsync(UserDTO model, string password);
        Task UpdateAsync(UserDTO model);
        Task ChangePasswordAsync(string userEmail, string newPassword);
        Task DeleteAsync(int id);
        Task<AuthenticationModelDTO> GetTokenAsync(string email, string password);
        Task<string> AssignRoleAsync(AssignRoleDTO model);
        int TryGetUserId(string email);
        Task<int> ValidateUserExist(int userId);

        Task<UserDTOAdmin> AdminGetUserAsync(int id);
        Task<List<UserDTOAdmin>> AdminGetUsersAsync(int id);
        Task UndoDeleteUserAsync(int id);
    }
}
