﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface ICarService
    {
        Task<CarDTO> GetCarAsync(int id);
        Task<ICollection<CarDTO>> GetAllCarsAsync();
        Task<ICollection<CarDTO>> GetAllCarsForUser(int Id);
        Task<CarDTO> CreateCarAsync(CarDTO carDTO);
        Task UpdateCarAsync(CarDTO carDTO);
        void ValidateCarExists(int carId);
    }
}
