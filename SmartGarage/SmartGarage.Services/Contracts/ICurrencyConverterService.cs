﻿using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Services.Contracts
{
    public interface ICurrencyConverterService
    {
        CurrencySettings GetCurrency(string desiredCurrency);
    }
}
