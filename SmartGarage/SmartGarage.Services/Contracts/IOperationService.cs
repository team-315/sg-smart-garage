﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IOperationService
    {
        Task<OperationDTO> GetOperationAsync(int id);
        Task<ICollection<OperationDTO>> GetAllOperationsAsync();
        Task<ICollection<OperationDTO>> GetAllOperationsByNameAndPriceAsync(string? name, int? price);
        Task<OperationDTO> CreateAsync(OperationDTO model);
        Task UpdateAsync(OperationDTO model);
  
        Task DeleteAsync(int id);
        void ValidateOperationsExist(List<int> ids);
    }
}
