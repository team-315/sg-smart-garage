﻿using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IMailService
    {
        Task SendWelcomeEmailAsync(string userName, string toEmail);
        Task SendForgotPasswordEmail(string userName, string url);

    }
}
