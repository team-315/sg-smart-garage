﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IVisitService
    {
        Task<VisitDTO> GetVisitAsync(int id);
        Task<ICollection<VisitDTO>> GetAllVisitsAsync();
        Task<List<VisitDTO>> GetAllOperationsFiltered(int userId, OperationsFilterDTO model = null);
        Task<HashSet<int>> GetAllUserIdsOrderedByVisitDate();
        Task<int> CreateAsync(int carId, List<int> ids);
        Task UpdateCarForVisitAsync(int id, int carId);
    }
}
