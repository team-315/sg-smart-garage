﻿using SmartGarage.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Contracts
{
    public interface IManufacturerService
    {
        Task<ManufacturerDTO> GetManufacturerAsync(int id);
        Task<ICollection<ManufacturerDTO>> GetAllManufacturersAsync();
        Task<int> CreateIfNotExistAsync(string name);
    }
}
