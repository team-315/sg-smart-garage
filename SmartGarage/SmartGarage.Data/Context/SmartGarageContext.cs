﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context.Configurations;
using SmartGarage.Data.Models;
using SmartGarage.Data.Seeder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Data.Context
{
    public class SmartGarageContext : IdentityDbContext<User,Role,int>
    {
        public SmartGarageContext(DbContextOptions<SmartGarageContext> options)
            : base(options)
        {

        }

        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<CarModel> CarModels { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public DbSet<Visit> Visits { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.ApplyConfiguration(new ServiceVisitConfig());

            modelBuilder.Seeder();

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
