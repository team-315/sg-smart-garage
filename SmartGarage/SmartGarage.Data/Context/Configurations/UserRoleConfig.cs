﻿//using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Metadata.Builders;
//using SmartGarage.Data.Models;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace SmartGarage.Data.Context.Configurations
//{
//    public class UserRoleConfig : IEntityTypeConfiguration<UserRole>
//    {
//        public void Configure(EntityTypeBuilder<UserRole> builder)
//        {
//            builder.HasKey(ur => new { ur.RoleId, ur.UserId });

//            builder.HasOne(cr => cr.User)
//                .WithMany(u => u.Roles)
//                .HasForeignKey(u => u.UserId);

//            builder.HasOne(cr => cr.Role)
//                .WithMany(c => c.Roles)
//                .HasForeignKey(c => c.RoleId);
//        }
//    }
//}
