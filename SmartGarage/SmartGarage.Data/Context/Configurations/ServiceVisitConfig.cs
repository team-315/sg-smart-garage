﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Data.Context.Configurations
{
    class ServiceVisitConfig : IEntityTypeConfiguration<OperationVisit>
    {
        public void Configure(EntityTypeBuilder<OperationVisit> builder)
        {
            builder.HasKey(sv => new { sv.OperationId, sv.VisitId });

            builder.HasOne(sv => sv.Operation)
                .WithMany(s => s.Visits)
                .HasForeignKey(c => c.OperationId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(sv => sv.Visit)
                .WithMany(s => s.Visits)
                .HasForeignKey(c => c.VisitId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
