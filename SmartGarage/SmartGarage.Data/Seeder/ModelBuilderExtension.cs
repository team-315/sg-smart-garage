﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Models;
using SmartGarage.Data.Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void Seeder(this ModelBuilder builder)
        {
            var opp = new Manufacturer() { Id = 1, Name = "Dodge" };
            builder.Entity<Manufacturer>().HasData(
               new Manufacturer[] {
                      opp,
                       new Manufacturer(){Id= 2 , Name = "Ford"},
                       new Manufacturer(){Id= 3 , Name = "BMW"},
                       new Manufacturer(){Id= 4 , Name = "Volkswagen"},
                       new Manufacturer(){Id= 5 , Name = "Nissan"},
                       new Manufacturer(){Id= 6 , Name = "Ferrari"},

               });
            var param = new CarModel() { Id = 1, Name = "Charger", ManufacturerId = opp.Id };
            builder.Entity<CarModel>().HasData(
                new CarModel[] {
                       param,
                       new CarModel(){Id= 2 , Name = "Mustang", ManufacturerId = 2},
                       new CarModel(){Id= 3 , Name = "7 Series", ManufacturerId = 3},
                       new CarModel(){Id= 4 , Name = "Passat", ManufacturerId = 4},
                       new CarModel(){Id= 5 , Name = "GTR", ManufacturerId = 5},
                       new CarModel(){Id= 6 , Name = "488 Pista", ManufacturerId = 6},
                       new CarModel(){Id= 7 , Name = "Challenger", ManufacturerId = 1}
                });

            builder.Entity<Role>().HasData(
                new Role[]
                {
                    new Role() {Id = 1, Name = Authorization.Roles.Customer.ToString(), NormalizedName = Authorization.Roles.Customer.ToString().ToUpper() , ConcurrencyStamp = "78dd0769-2285-4951-a678-0hg44af47b34" },
                    new Role() {Id = 2, Name = Authorization.Roles.Employee.ToString(), NormalizedName = Authorization.Roles.Employee.ToString().ToUpper() , ConcurrencyStamp = "12fg0762-5485-7645-a678-0hg44af11b22"  },
                    new Role() {Id = 3,Name = Authorization.Roles.Owner.ToString(), NormalizedName = Authorization.Roles.Owner.ToString().ToUpper() , ConcurrencyStamp = "56dh0769-2285-4543-a456-0hg44ah76b31"  }
                });

            builder.Entity<Operation>().HasData(
                new Operation[]
                {
                    new Operation() {Id = 1, Name = "Oil Change", PriceValue = 80},
                    new Operation() {Id = 2, Name = "Filter Change", PriceValue = 125},
                    new Operation() {Id = 3, Name = "Tire Change", PriceValue = 60},
                    new Operation() {Id = 4, Name = "Engine Fix", PriceValue = 1500},
                });

            var userA = new User
            {
                Id = 1,
                UserName = Authorization.default_FirstName,
                LastName = Authorization.default_LastName,
                PhoneNumber = Authorization.default_phone,
                Email = Authorization.default_email,
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                NormalizedEmail = Authorization.default_email.ToUpper(),
                SecurityStamp = "15CLJEKQCTLPRXMVXXNSWXZH6R6KJRRU",
                ConcurrencyStamp = "f543d523-2dad-47d2-a43e-2c04519769ef"
            };
            var userB = new User()
            {
                Id = 2,
                Email = "petur@ivanov.com",
                UserName = "Petur",
                LastName = "Ivanov",
                PhoneNumber = "0888966699",
                NormalizedEmail = "petur@ivanov.com".ToUpper(),
                SecurityStamp = "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN",
                ConcurrencyStamp = "90de0769-2285-4951-a340-0ac36af47b19"
            };
            var userC = new User()
            {
                Id = 3,
                Email = "georgiP@abv.com",
                UserName = "Georgi",
                LastName = "Petrov",
                PhoneNumber = "0888774447",
                NormalizedEmail = "georgiP@abv.com".ToUpper(),
                SecurityStamp = "1I4VNHIJTDFNGF3KDJHNFUV5PPAIHGXN",
                ConcurrencyStamp = "11de0369-1383-4951-1140-0ac96af37b16"
            };
            var userD = new User()
            {
                Id = 4,
                Email = "mihaelaN@abv.com",
                UserName = "Mihaela",
                LastName = "Nenova",
                PhoneNumber = "0888774447",
                NormalizedEmail = "mihaelaN@abv.com".ToUpper(),
                SecurityStamp = "165POUIJTDFNGF3KDPANRUV1PPA34GXN",
                ConcurrencyStamp = "16r10369-6458-4151-8540-0gf87af34b33"
            };

            var passwordHasher = new PasswordHasher<User>();

            var passA = passwordHasher.HashPassword(userA, Authorization.default_password);
            var passB = passwordHasher.HashPassword(userB, "pe6o1234");
            var passC = passwordHasher.HashPassword(userC, "georgi12");
            var passD = passwordHasher.HashPassword(userC, "mihaela1");

            userA.PasswordHash = passA;
            userB.PasswordHash = passB;
            userC.PasswordHash = passC;
            userD.PasswordHash = passD;

            builder.Entity<User>().HasData(userA, userB, userC, userD);

            builder.Entity<Car>().HasData(new Car()
            {
                Id = 1,
                UserId = 2,
                CarModelId = 1,
                RegistrationPlate = "RB2342AS",
                VIN = "23423468439205736"
            });

            builder.Entity<Car>().HasData(new Car()
            {
                Id = 2,
                UserId = 3,
                CarModelId = 4,
                RegistrationPlate = "PA2129NM",
                VIN = "41641232902341234"
            });

            builder.Entity<Car>().HasData(new Car()
            {
                Id = 3,
                UserId = 4,
                CarModelId = 5,
                RegistrationPlate = "CA2129KP",
                VIN = "43699233302348634"
            });

            builder.Entity<Car>().HasData(new Car()
            {
                Id = 4,
                UserId = 2,
                CarModelId = 5,
                RegistrationPlate = "CA2129KP",
                VIN = "41111233772348695"
            });

            builder.Entity<UserRole>().HasData(
                new UserRole() { RoleId = 2, UserId = 1 },
                new UserRole() { RoleId = 1, UserId = 2 },
                new UserRole() { RoleId = 1, UserId = 3 },
                new UserRole() { RoleId = 1, UserId = 4 });

            builder.Entity<Visit>().HasData(
                new Visit[]
                {
                    new Visit() {Id = 1, CarId = 1, Date = DateTime.Now},
                    new Visit() {Id = 2, CarId = 2, Date = DateTime.Now.AddDays(-1)},
                    new Visit() {Id = 3, CarId = 1, Date = DateTime.Now.AddDays(-3)},
                    new Visit() {Id = 4, CarId = 3, Date = DateTime.Now.AddDays(-5)},
                    new Visit() {Id = 5, CarId = 3, Date = DateTime.Now.AddDays(-1)},
                    new Visit() {Id = 6, CarId = 4, Date = DateTime.Now.AddDays(-1)}
                });

            builder.Entity<OperationVisit>().HasData(
                new OperationVisit() { OperationId = 1, VisitId = 1 },
                new OperationVisit() { OperationId = 2, VisitId = 1 },
                new OperationVisit() { OperationId = 1, VisitId = 2 },
                new OperationVisit() { OperationId = 2, VisitId = 2 },
                new OperationVisit() { OperationId = 3, VisitId = 3 },
                new OperationVisit() { OperationId = 1, VisitId = 4 },
                new OperationVisit() { OperationId = 2, VisitId = 4 },
                new OperationVisit() { OperationId = 3, VisitId = 4 },
                new OperationVisit() { OperationId = 1, VisitId = 5 },
                new OperationVisit() { OperationId = 3, VisitId = 5 },
                new OperationVisit() { OperationId = 1, VisitId = 6 },
                new OperationVisit() { OperationId = 4, VisitId = 6 }
                );
        }
    }
}
