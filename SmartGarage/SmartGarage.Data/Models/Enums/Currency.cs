﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Data.Models.Enums
{
    public enum Currency
    {
        BGN,
        USD,
        EUR
    }
}
