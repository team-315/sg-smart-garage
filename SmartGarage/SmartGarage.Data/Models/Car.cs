﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RegistrationPlate { get; set; }
        [Required]
        public string VIN { get; set; }
        public int CarModelId { get; set; }
        public CarModel CarModel { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime? IsDeleted { get; set; }
        public ICollection<Visit> Visits { get; set; } = new List<Visit>();
    }
}
