﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class User : IdentityUser<int>
    {
        
        [Required]
        public override string UserName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public override string Email { get; set; }
        
        [Required, StringLength(10)] 
        public override string PhoneNumber { get; set; }
        public DateTime? IsDeleted { get; set; }
        public ICollection<Car> Cars { get; set; } = new List<Car>();
        
    }
}
