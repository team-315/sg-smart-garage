﻿using SmartGarage.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Operation
    {
        double priceValue;
        [Key]
        public int Id { get; set; }
        [Required, MinLength(2), MaxLength(123)]
        public string Name { get; set; }
        
        [Required, Range(0, double.MaxValue)]
        public double PriceValue
        {
            get => this.priceValue;
            set
            {
                if (value < 0)
                {
                    throw new ValidationException("Price can not me below 0");
                }
                this.priceValue = value;
            }
        }
        
        public DateTime? IsDeleted { get; set; }
        
        public List<OperationVisit> Visits { get; set; }
    }
}
