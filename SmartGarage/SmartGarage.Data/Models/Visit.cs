﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Data.Models
{
    public class Visit
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public int CarId { get; set; }
        public Car Car { get; set; }
        public double TotalPrice { get; set; }
        public List<OperationVisit> Visits { get; set; }
    }
}
