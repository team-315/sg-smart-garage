﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Data.Models.Constants
{
    public class Authorization
    {
        public enum Roles
        {
            Owner,
            Employee,
            Customer
        }
        public const string default_FirstName = "Ivan";
        public const string default_LastName = "Borovanski";
        public const string default_email = "IvanB@abv.bg";
        public const string default_password = "PassWord";
        public const string default_phone = "0888999888";
        public const Roles default_role = Roles.Customer;
    }
}
