﻿namespace SmartGarage.Data.Models
{
    public class OperationVisit
    {
        public int VisitId { get; set; }
        public Visit Visit { get; set; }
        public int OperationId { get; set; }
        public Operation Operation { get; set; }
    }
}
