﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarModelServiceTests
{
    [TestClass]
    public class CreateCarModel_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task CreateCorrectCarModel_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(CreateCorrectCarModel_When_ParamsAreValid));
            var manufacturer = new Manufacturer { Id = 1, Name = "Mercedes" };
            var carModelDTO = new CarModelDTO { Id = 1, Name = "CLS", ManufacturerId = 1 };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarModelService(assertContext, mapper);
                assertContext.Manufacturers.Add(manufacturer);
                assertContext.SaveChanges();

                var result = await sut.CreateCarModelIfNotExistAsync(carModelDTO.Name, carModelDTO.ManufacturerId);
                var model = await assertContext.CarModels.FirstOrDefaultAsync(c => c.Id == result);

                Assert.AreEqual(carModelDTO.Name, model.Name);
                Assert.AreEqual(carModelDTO.ManufacturerId, model.ManufacturerId);
            }
        }
        //[TestMethod]
        //public async Task ThrowExceptionAndCorrectMessage_When_CarModelAlreadyExists()
        //{
        //    options = Utils.GetOptions(nameof(ThrowExceptionAndCorrectMessage_When_CarModelAlreadyExists));

        //    var manufacturer = new Manufacturer { Id = 1, Name = "Mercedes" };
        //    var carModelDTO = new CarModelDTO { Id = 1, Name = "CLS", ManufacturerId = 1 };

        //    using (var arrangeContext = new SmartGarageContext(options))
        //    {
        //        var sut = new CarModelService(arrangeContext, mapper);
        //        arrangeContext.Manufacturers.Add(manufacturer);
        //        arrangeContext.SaveChanges();

        //        var result = await sut.CreateCarModelIfNotExistAsync(carModelDTO.Name, carModelDTO.ManufacturerId);
        //    }

        //    using (var assertContext = new SmartGarageContext(options))
        //    {
        //        var sut = new CarModelService(assertContext, mapper);
        //        string expectedExceptionMessage = $"Car model {carModelDTO.Name} already exists";

        //        ResourceAlreadyExistException realException = await Assert
        //            .ThrowsExceptionAsync<ResourceAlreadyExistException>(() 
        //            => sut.CreateCarModelIfNotExistAsync(carModelDTO.Name, carModelDTO.ManufacturerId));

        //        Assert.AreEqual(expectedExceptionMessage, realException.Message);
        //    }
        //}


    }
}
