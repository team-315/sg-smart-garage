﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Services.Mapper;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Service.Tests.CarModelServiceTests
{
    [TestClass]
    public class Constructor_Should : BaseTest
    {

        [TestMethod]
        public void CreateInstance()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CreateInstance));

            using (var arrangeContext = new SmartGarageContext(options))
            {
                //Act
                var sut = new CarModelService(arrangeContext, mapper);
                //Assert
                Assert.IsNotNull(sut);
            }
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenAnyArgumentIsNull()
        {
            var options = Utils.GetOptions(nameof(CreateInstance));
            var arrangeContext = new SmartGarageContext(options);

            Assert.ThrowsException<ArgumentNullException>
                (() => new CarModelService(arrangeContext, null));

            Assert.ThrowsException<ArgumentNullException>
                (() => new CarModelService(null, mapper));
        }
    }
}
