﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarModelServiceTests
{
    [TestClass]
    public class GetCarModel_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ReturnCorrectCarModel_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(ReturnCorrectCarModel_When_ParamsAreValid));
            var manufacturer = new Manufacturer { Id = 1, Name = "BMW" };
            var carModel = new CarModel { Id = 1, Name = "X6", ManufacturerId = manufacturer.Id };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarModelService(assertContext, mapper);
                var result = await sut.GetCarModelAsync(carModel.Id);

                Assert.AreEqual(carModel.Id, result.Id);
                Assert.AreEqual(carModel.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_CarModelIsNotFound()
        {
            options = Utils.GetOptions(nameof(ThrowExceptionAndCorrectMessage_CarModelIsNotFound));

            using (var assertContext = new SmartGarageContext(options))
            {

                var sut = new CarModelService(assertContext, mapper);
                const int NOT_VALID_CARMODEL_ID = -1;
                string expectedExceptionMessage = "Car model does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetCarModelAsync(NOT_VALID_CARMODEL_ID));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

    }
}
