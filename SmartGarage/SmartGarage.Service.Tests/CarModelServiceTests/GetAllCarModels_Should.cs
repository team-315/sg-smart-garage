﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarModelServiceTests
{
    [TestClass]
    public class GetAllCarModels_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ReturnCollectionOfCarModelDTOs()
        {
            // Arrange
            options = Utils.GetOptions(nameof(ReturnCollectionOfCarModelDTOs));

            var carModels = new List<CarModel>()
            {
                new CarModel { Id = 1, Name = "Camaro", Manufacturer = new Manufacturer{Id = 1, Name="Chevrolet"}, ManufacturerId=1 },
                new CarModel { Id = 2, Name = "Viper", Manufacturer = new Manufacturer{Id = 2, Name="Dodge"}, ManufacturerId=2 },
                new CarModel { Id = 3, Name = "Mustang",Manufacturer = new Manufacturer{Id = 3, Name="Ford"}, ManufacturerId=3 },
            };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.CarModels.AddRange(carModels);
                arrangeContext.SaveChanges();
            }

            // Act & Assert
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarModelService(assertContext, mapper);
                var result = await sut.GetAllCarModelsAsync();

                Assert.IsInstanceOfType(result, typeof(ICollection<CarModelDTO>));
                Assert.AreEqual(3, result.Count);

                Assert.AreEqual(carModels[0].Id, result.First().Id);
                Assert.AreEqual(carModels[0].Name, result.First().Name);
                Assert.AreEqual(carModels[2].Id, result.Last().Id);
                Assert.AreEqual(carModels[2].Name, result.Last().Name);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_CarModels_NotFound()
        {
            options = Utils.GetOptions(nameof(ThrowExceptionAndCorrectMessage_When_CarModels_NotFound));

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarModelService(assertContext, mapper);
                string expectedExceptionMessage = "There are no registered car models";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetAllCarModelsAsync());
                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

    }
}
