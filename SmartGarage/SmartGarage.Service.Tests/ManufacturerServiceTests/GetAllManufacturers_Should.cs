﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class GetAllManufacturers_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_Manufacturers_NotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(assertContext, mapper);
                string expectedExceptionMessage = "There are no registered manufacturers";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetAllManufacturersAsync());
                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollection()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var manufacturerA = new Manufacturer { Id = 1, Name = "Honda" };
            var manufacturerB = new Manufacturer { Id = 2, Name = "Toyota" };

            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Manufacturers.Add(manufacturerA);
                arrangecontext.Manufacturers.Add(manufacturerB);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(assertcontext, mapper);
                var result = await sut.GetAllManufacturersAsync();

                Assert.IsInstanceOfType(result, typeof(List<ManufacturerDTO>));
                Assert.AreEqual(2, result.Count);

                Assert.AreEqual(manufacturerA.Id, result.First().Id);
                Assert.AreEqual(manufacturerA.Name, result.First().Name);
                Assert.AreEqual(manufacturerB.Id, result.ToList()[1].Id);
                Assert.AreEqual(manufacturerB.Name, result.ToList()[1].Name);
            }
        }
    }
}
