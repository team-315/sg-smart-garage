using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using SmartGarage.Services.Services;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class GetManufacturer_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        private Mock<IDTOMapper> mapperr;

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_WhenManufacturerIsNotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            

            using (var assertcontext = new SmartGarageContext(options))
            {

                var sut = new ManufacturerService(assertcontext, mapper);
                const int invalidId = -1;
                string expectedExceptionMessage = "Manufacturer does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetManufacturerAsync(invalidId));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectManufacturerDTO_WhenParametersAreCorrect()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            mapperr = new Mock<IDTOMapper>();

            var manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Manufacturers.Add(manufacturer);
                arrangecontext.SaveChanges();
            }

            var manufacturerDTO= new ManufacturerDTO() { Id = 1, Name = "Honda" };

            mapperr.Setup(x => x.ManufacturerToDTO(It.IsAny<Manufacturer>())).Returns(manufacturerDTO);

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(assertcontext, mapperr.Object);
                var result = await sut.GetManufacturerAsync(manufacturer.Id);

                Assert.AreEqual(manufacturer.Id, result.Id);
                Assert.AreEqual(manufacturer.Name, result.Name);
            }
        }
    }
}
