﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Mapper;
using SmartGarage.Services.Services;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class CreateManufacturer_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        //[TestMethod]
        //public async Task ThrowExceptionAndCorrectMessage_When_ManufacturerAlreadyExists()
        //{
        //    options = Utils.GetOptions(nameof(TestContext.TestName));

        //    var manufacturerDTO = new ManufacturerDTO { Id = 1, Name = "Honda" };

        //    using (var arrangeContext = new SmartGarageContext(options))
        //    {
        //        var sut = new ManufacturerService(arrangeContext, mapper);
        //        var result = await sut.CreateIfNotExistAsync(manufacturerDTO.Name);
        //    }

        //    using (var assertContext = new SmartGarageContext(options))
        //    {
        //        var sut = new ManufacturerService(assertContext, mapper);
        //        string expectedExceptionMessage = $"Manufacturer {manufacturerDTO.Name} already exists";

        //        ResourceAlreadyExistException realException = await Assert
        //            .ThrowsExceptionAsync<ResourceAlreadyExistException>(
        //            () => sut.CreateIfNotExistAsync(manufacturerDTO.Name));

        //        Assert.AreEqual(expectedExceptionMessage, realException.Message);
        //    }
        //}

        [TestMethod]
        public async Task CreateCorrectManufacturer_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var manufacturerDTO = new ManufacturerDTO { Id = 1, Name = "Honda" };// check capitalization

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(assertContext, mapper);
                var result = await sut.CreateIfNotExistAsync(manufacturerDTO.Name);

                Assert.AreEqual(manufacturerDTO.Id, result);
            }
        }
    }
}
