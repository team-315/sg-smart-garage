﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.UserServiceTests
{
    [TestClass]
    public class UpdateUser_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Mock<IUserStore<User>> _userStore = new Mock<IUserStore<User>>();
        private Mock<IRoleStore<Role>> _roleStore = new Mock<IRoleStore<Role>>();

        private Mock<UserManager<User>> _userManager;
        private Mock<RoleManager<Role>> _roleManager;
        private Mock<IOptions<JWT>> _jwt;

        private User user;
        private User userToUpdate;
        private User userDel;

        [TestInitialize()]
        public async Task Initialize()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            _jwt = new Mock<IOptions<JWT>>();
            _roleManager = new Mock<RoleManager<Role>>(_roleStore.Object, null, null, null, null);
            _userManager = new Mock<UserManager<User>>(
            _userStore.Object, null, null, null, null, null, null, null, null);

            user = new User { Id = 1, PhoneNumber = "0979146521", Email = "testtt@gmail.com", UserName = "Pesho", LastName = "Petrov", IsDeleted = null };
            userDel = new User { Id = 2, PhoneNumber = "0888999666", Email = "deleted@gmail.com", UserName = "deleted", IsDeleted = DateTime.Now };
            userToUpdate = new User { Id = 3, PhoneNumber = "0898743412", Email = "Ivan@gmail.com", UserName = "Ivan", LastName = "Petkov", IsDeleted = null };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Add(user);
                arrangeContext.Add(userToUpdate);
                arrangeContext.Add(userDel);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_UserDoesNotExist()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var userToUpdateDTO = new UserDTO { Id = -1, PhoneNumber = "0979146521", Email = "Ivan@gmail.com", UserName = "Ivan", LastName = "Petkov", IsDeleted = null };
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.UpdateAsync(userToUpdateDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_UserPhoneAlreadyNotExist()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var userToUpdateDTO = new UserDTO { Id = 3, PhoneNumber = "0979146521", Email = "Ivan@gmail.com", UserName = "Ivan", LastName = "Petkov", IsDeleted = null };
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User phone {user.PhoneNumber} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.UpdateAsync(userToUpdateDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_UserNameAlreadyNotExist()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var userToUpdateDTO = new UserDTO { Id = 3, PhoneNumber = "0892457651", Email = "Ivan@gmail.com", UserName = "Pesho", LastName = "Petrov", IsDeleted = null };
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User names {userToUpdateDTO.UserName} {userToUpdateDTO.LastName} already exists together";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.UpdateAsync(userToUpdateDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_EmailAlreadyNotExist()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var userToUpdateDTO = new UserDTO { Id = 3, PhoneNumber = "0979146521", Email = "testtt@gmail.com", UserName = "Ivan", LastName = "Petkov", IsDeleted = null };
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User Email {user.Email} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.UpdateAsync(userToUpdateDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task UpdateUser_When_ParamsAreValid()
        {
            var userTest = new User { };

            _userManager.Setup(x => x.UpdateAsync(It.IsAny<User>())).Returns(Task.FromResult(IdentityResult.Success))
                .Callback<User>((user) => { userTest = user; });

            using (var assertContext = new SmartGarageContext(options))
            {
                var userToUpdateDTO = new UserDTO { Id = 3, PhoneNumber = "0892457651", Email = "Ivan@gmail.com", UserName = "Ivan", LastName = "Petkov", IsDeleted = null };
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                await sut.UpdateAsync(userToUpdateDTO);

                var updatedUser = assertContext.Users.Find(userToUpdateDTO.Id);

                Assert.AreEqual(userToUpdateDTO.UserName, userTest.UserName);
                Assert.AreEqual(userToUpdateDTO.Email, userTest.Email);
                Assert.AreEqual(userToUpdateDTO.PhoneNumber, userTest.PhoneNumber);
                Assert.AreEqual(userToUpdateDTO.Email, userTest.Email);
            }

            _userManager.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Once);
        }
    }
}
