﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Services;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.UserServiceTests
{
    [TestClass]
    public class GetAllUsers_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Mock<IUserStore<User>> _userStore = new Mock<IUserStore<User>>();
        private Mock<IRoleStore<Role>> _roleStore = new Mock<IRoleStore<Role>>();

        private Mock<UserManager<User>> _userManager;
        private Mock<RoleManager<Role>> _roleManager;
        private Mock<IOptions<JWT>> _jwt;
        private User test;
        private User testDeleted;

        [TestInitialize()]
        public void Initialize()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            _jwt = new Mock<IOptions<JWT>>();
            _roleManager = new Mock<RoleManager<Role>>(_roleStore.Object, null, null, null, null);
            _userManager = new Mock<UserManager<User>>(
            _userStore.Object, null, null, null, null, null, null, null, null);

            testDeleted = new User { Id = 2, PhoneNumber = "0898743412", Email = "testtt@gmail.com", UserName = "Pesho", IsDeleted = DateTime.Now};

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Add(testDeleted);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnEmpty_When_NotFound()
        {
            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertcontext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                var result = await sut.GetAllUsersAsync();

                Assert.IsFalse(result.Any());
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollection_NotReturn_DeletedOperations()
        {
            test = new User { Id = 1, PhoneNumber = "0898763412", Email = "test@gmail.com", UserName = "Bobo", IsDeleted = null };
            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Add(test);
                arrangeContext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertcontext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                var result = await sut.GetAllUsersAsync();

                Assert.IsInstanceOfType(result, typeof(List<UserDTO>));
                Assert.AreEqual(1, result.Count);

                Assert.AreEqual(test.Id, result.First().Id);
                Assert.AreEqual(test.UserName, result.First().UserName);
                Assert.AreEqual(test.Email, result.First().Email);
            }
        }
    }
}
