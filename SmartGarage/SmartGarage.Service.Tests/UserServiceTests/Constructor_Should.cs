﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Services;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Service.Tests.UserServiceTests
{
    [TestClass]
    public class Constructor_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Mock<IUserStore<User>> _userStore = new Mock<IUserStore<User>>();
        private Mock<IRoleStore<Role>> _roleStore = new Mock<IRoleStore<Role>>();

        private Mock<UserManager<User>> _userManager;
        private Mock<RoleManager<Role>> _roleManager;
        private Mock<IOptions<JWT>> _jwt;

        [TestMethod]
        public void CreateInstance()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new SmartGarageContext(options);
            _jwt = new Mock<IOptions<JWT>>();
            _roleManager = new Mock<RoleManager<Role>>(_roleStore.Object, null, null, null, null);
            _userManager = new Mock<UserManager<User>>(
            _userStore.Object, null, null, null, null, null, null, null, null);

            var sut = new UserService(context, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);

            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenAnyArgumentIsNull()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new SmartGarageContext(options);

            Assert.ThrowsException<NullReferenceException>
                (() => new UserService(context, null, _userManager.Object, _roleManager.Object, _jwt.Object));

            Assert.ThrowsException<NullReferenceException>
                (() => new UserService(null, mapper, _userManager.Object, _roleManager.Object, _jwt.Object));

            Assert.ThrowsException<NullReferenceException>
                (() => new UserService(context, mapper, null, _roleManager.Object, _jwt.Object));
           
            Assert.ThrowsException<NullReferenceException>
                (() => new UserService(context, mapper, _userManager.Object, null, _jwt.Object));
            
            Assert.ThrowsException<NullReferenceException>
                (() => new UserService(context, mapper, _userManager.Object, _roleManager.Object, null));
        }
    }
}
