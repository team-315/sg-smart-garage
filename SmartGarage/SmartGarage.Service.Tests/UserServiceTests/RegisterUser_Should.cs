﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Data.Models.Constants;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.UserServiceTests
{
    [TestClass]
    public class RegisterUser_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Mock<IUserStore<User>> _userStore = new Mock<IUserStore<User>>();
        private Mock<IRoleStore<Role>> _roleStore = new Mock<IRoleStore<Role>>();

        private Mock<UserManager<User>> _userManager;
        private Mock<RoleManager<Role>> _roleManager;
        private Mock<IOptions<JWT>> _jwt;
        private User user;
        private User userDel;

        [TestInitialize()]
        public async Task Initialize()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            _jwt = new Mock<IOptions<JWT>>();
            _roleManager = new Mock<RoleManager<Role>>(_roleStore.Object, null, null, null, null);
            _userManager = new Mock<UserManager<User>>(_userStore.Object, null, null, null, null, null, null, null, null);

            user = new User { Id = 1, LastName = "Petrov", PhoneNumber = "0898743412", Email = "testtt@gmail.com", UserName = "Pesho", IsDeleted = null};
            userDel = new User { Id = 2, LastName = "Petrov", PhoneNumber = "0888999666", Email = "deleted@gmail.com", UserName = "deleted", IsDeleted = DateTime.Now };
            
            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Add(user);
                arrangeContext.Add(userDel);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_UserNameAlreadyExists()
        {
            var testDTO = new UserDTO { Id = 3, LastName = "Petrov", PhoneNumber = "0888999666", Email = "deleted@gmail.com", UserName = "Pesho", IsDeleted = null };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User names {testDTO.UserName} {testDTO.LastName} already exists together";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.RegisterAsync(testDTO, "password"));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_EmailAlreadyExists()
        {

            var testDTO = new UserDTO { Id = 3, PhoneNumber = "0888999666", Email = "testtt@gmail.com", UserName = "deleted", LastName = "deleted", IsDeleted = null };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User Email {testDTO.Email} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.RegisterAsync(testDTO, "password"));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_PhoneAlreadyExists()
        {

            var testDTO = new UserDTO { Id = 3, PhoneNumber = "0898743412", Email = "deleted@gmail.com", UserName = "deleted", LastName = "deleted", IsDeleted = null};

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User phone {testDTO.PhoneNumber} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.RegisterAsync(testDTO, "password"));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task CreateCorrectUser_When_ParamsAreValid()
        {
            var testDTO = new UserDTO { Id = 3,LastName = "Svobodno", PhoneNumber = "0888999666", Email = "deleted@gmail.com", UserName = "deleted", IsDeleted = null};

            var userTestB = new User{ };
            var passTest = string.Empty;
            var role = string.Empty;

            _userManager.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success))
                .Callback<User, string>((user, r) => { userTestB = user; role = r; });

            using (var assertContext = new SmartGarageContext(options))

            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);

                var result = await sut.RegisterAsync(testDTO, "password");


                Assert.AreEqual(testDTO.LastName, userTestB.LastName );
                Assert.AreEqual(testDTO.LastName, userTestB.LastName );
                Assert.AreEqual(testDTO.Email, userTestB.Email);
                Assert.AreEqual(testDTO.PhoneNumber, userTestB.PhoneNumber);
                Assert.AreEqual(Authorization.default_role.ToString(), role);
            }

            _userManager.Verify(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()),Times.Once);

        }
    }
}
