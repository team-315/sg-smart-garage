﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.UserServiceTests
{
    [TestClass]
    public class AssignRole_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Mock<IUserStore<User>> _userStore = new Mock<IUserStore<User>>();
        private Mock<IRoleStore<Role>> _roleStore = new Mock<IRoleStore<Role>>();

        private Mock<UserManager<User>> _userManager;
        private Mock<RoleManager<Role>> _roleManager;
        private Mock<IOptions<JWT>> _jwt;
        private User user;
        private User userDel;

        [TestInitialize()]
        public async Task Initialize()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            _jwt = new Mock<IOptions<JWT>>();
            _roleManager = new Mock<RoleManager<Role>>(_roleStore.Object, null, null, null, null);
            _userManager = new Mock<UserManager<User>>(_userStore.Object, null, null, null, null, null, null, null, null);

            user = new User { Id = 1, LastName = "Petrov", PhoneNumber = "0898743412", Email = "testtt@gmail.com", UserName = "Pesho", IsDeleted = null };
            userDel = new User { Id = 2, LastName = "Petrov", PhoneNumber = "0888999666", Email = "deleted@gmail.com", UserName = "deleted", IsDeleted = DateTime.Now };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Add(user);
                arrangeContext.Add(userDel);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_EmailDoesNotExists()
        {
            var assignDTO = new AssignRoleDTO { Email = "testtt@gmail.com", Role = "Employee" };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"No Accounts Registered with {assignDTO.Email}.";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.AssignRoleAsync(assignDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }
    }
}
