﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using SmartGarage.Services.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.UserServiceTests
{
    [TestClass]
    public class DeleteUser_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Mock<IUserStore<User>> _userStore = new Mock<IUserStore<User>>();
        private Mock<IRoleStore<Role>> _roleStore = new Mock<IRoleStore<Role>>();

        private Mock<UserManager<User>> _userManager;
        private Mock<RoleManager<Role>> _roleManager;
        private Mock<IOptions<JWT>> _jwt;
        private User user;
        private User userDel;

        [TestInitialize()]
        public async Task Initialize()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            _jwt = new Mock<IOptions<JWT>>();
            _roleManager = new Mock<RoleManager<Role>>(_roleStore.Object, null, null, null, null);
            _userManager = new Mock<UserManager<User>>(
            _userStore.Object, null, null, null, null, null, null, null, null);

            user = new User { Id = 1, PhoneNumber = "0898743412", Email = "testtt@gmail.com", UserName = "Pesho", IsDeleted = null };
            userDel = new User { Id = 2, PhoneNumber = "0888999666", Email = "deleted@gmail.com", UserName = "deleted", IsDeleted = DateTime.Now };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Add(user);
                arrangeContext.Add(userDel);
                await arrangeContext.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_UserNotFound()
        {
            // Act & Assert
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                const int testId = -1;
                string expectedExceptionMessage = $"User does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.DeleteAsync(testId));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_UserIsAlreadyDeleted()
        {
            // Act & Assert
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                string expectedExceptionMessage = $"User does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.DeleteAsync(userDel.Id));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task DeleteWithCorrectParameters()
        {
            var userTest = new User { };

            _userManager.Setup(x => x.UpdateAsync(It.IsAny<User>())).Returns(Task.FromResult(IdentityResult.Success))
                .Callback<User>((user) => { userTest = user; });

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new UserService(assertContext, mapper, _userManager.Object, _roleManager.Object, _jwt.Object);
                await sut.DeleteAsync(user.Id);

                Assert.AreEqual(user.Email, userTest.Email);
            }

            _userManager.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Once);
        }
    }
}
