﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarServiceTests
{
    [TestClass]
    public class GetCar_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ReturnCorrectCar_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(ReturnCorrectCar_When_ParamsAreValid));
            var manufacturer = new Manufacturer { Id = 1, Name = "BMW" };
            var carModel = new CarModel { Id = 1, Name = "X6", ManufacturerId = manufacturer.Id };
            var user = new User { Id = 1, UserName = "Petko", Email = "petkov@gmail.com", PhoneNumber = "0979146521" };
            var car = new Car { Id = 1, UserId = 1, CarModelId = 1, RegistrationPlate = "ST0008VR", VIN = "253412300534543534" };
            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.Users.Add(user);
                arrangeContext.Cars.Add(car);
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarService(assertContext, mapper);
                var result = await sut.GetCarAsync(car.Id);

                Assert.AreEqual(car.Id, result.Id);
                Assert.AreEqual(car.RegistrationPlate, result.RegistrationPlate);
                Assert.AreEqual(car.UserId, result.UserId);
                Assert.AreEqual(car.CarModelId, result.CarModelId);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_WhenCarIsNotFound()
        {
            options = Utils.GetOptions(nameof(ThrowExceptionAndCorrectMessage_WhenCarIsNotFound));

            using (var assertcontext = new SmartGarageContext(options))
            {

                var sut = new CarService(assertcontext, mapper);
                const int NOT_VALID_CAR_ID = -1;
                string expectedExceptionMessage = "Car does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetCarAsync(NOT_VALID_CAR_ID));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }
    }
}
