﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarServiceTests
{
    [TestClass]
    public class UpdateCar_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Manufacturer manufacturer;
        private CarModel carModel;
        private User user;
        private Car car;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            options = Utils.GetOptions(nameof(TestContext.TestName));
            manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            carModel = new CarModel { Id = 1, ManufacturerId = manufacturer.Id, Name = "Civik" };
            user = new User { Id = 1, Email = "petur@slabeev.com", UserName = "petur", PhoneNumber = "0898743412" };
            car = new Car { Id = 1, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB2342AS", VIN = "234234"};

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.Users.Add(user);
                arrangeContext.Cars.Add(car);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task UpdateCar_When_ParamsAreCorrect()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            // Arrange
            var carDTO = new CarDTO
            {
                Id = car.Id,
              //  UserId = car.UserId,
                CarModelId = car.CarModelId,
                RegistrationPlate = car.RegistrationPlate,
                VIN = car.VIN
            };

            // Act
            using (var act = new SmartGarageContext(options))
            {
                var sut = new CarService(act, mapper);
                await sut.UpdateCarAsync(carDTO);
            }

            // Assert
            using (var assertContext = new SmartGarageContext(options))
            {
                var updatedCar = assertContext.Cars.Find(car.Id);

                Assert.AreEqual(carDTO.VIN, updatedCar.VIN);
                Assert.AreEqual(carDTO.RegistrationPlate, updatedCar.RegistrationPlate);
                //Assert.AreEqual();
            }
        }
    }
}
