﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarServiceTests
{
    [TestClass]
    public class GetAllCars_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ReturnCollectionOfCarDTOs()
        {
            // Arrange
            options = Utils.GetOptions(nameof(ReturnCollectionOfCarDTOs));

            var manufacturers = new List<Manufacturer>()
            {
                new Manufacturer() { Id = 1, Name = "BMW" },
                new Manufacturer() { Id = 2, Name = "Opel" },
                new Manufacturer() { Id = 3, Name = "Audi" },
            };

            var carModels = new List<CarModel>()
            {
                new CarModel { Id = 1, Name = "X6", ManufacturerId=1 },
                new CarModel { Id = 2, Name = "Omega", ManufacturerId=2 },
                new CarModel { Id = 3, Name = "Q7", ManufacturerId=3 },
            };

            var users = new List<User>()
            {
                new User { Id = 1, UserName = "Petko", Email = "petkov@gmail.com",  PhoneNumber = "0979146521" },
                new User { Id = 2, UserName = "Ivan", Email = "ivanov@gmail.com",  PhoneNumber = "0976996782" },
                new User { Id = 3, UserName = "Georgi", Email = "georgiev@gmail.com", PhoneNumber = "0979376437" },
            };
            var cars = new List<Car>()
            {
                new Car { Id = 1, UserId = 1, CarModelId = 1, RegistrationPlate = "RB2342AS", VIN = "253412331534543534" },
                new Car { Id = 2, UserId = 2, CarModelId = 2, RegistrationPlate = "SA2552SB", VIN = "253412331534265324" },
                new Car { Id = 3, UserId = 3, CarModelId = 3, RegistrationPlate = "ST1342VR", VIN = "253412331536798877" },
            };

            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Manufacturers.AddRange(manufacturers);
                arrangecontext.CarModels.AddRange(carModels);
                arrangecontext.Users.AddRange(users);
                arrangecontext.Cars.AddRange(cars);
                arrangecontext.SaveChanges();
            }

            // Act & Assert
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarService(assertContext, mapper);
                var result = await sut.GetAllCarsAsync();

                Assert.IsInstanceOfType(result, typeof(ICollection<CarDTO>));
                Assert.AreEqual(3, result.Count);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_Cars_NotFound()
        {
            options = Utils.GetOptions(nameof(ThrowExceptionAndCorrectMessage_When_Cars_NotFound));

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarService(assertContext, mapper);
                string expectedExceptionMessage = "There are no registered cars";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetAllCarsAsync());
                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

    }
}
