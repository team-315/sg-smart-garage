﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.CarServiceTests
{
    [TestClass]
    public class CreateCar_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Manufacturer manufacturer;
        private CarModel carModel;
        private User user;
        private CarDTO carDTO;

        [TestMethod]
        public async Task CreateCorrectCar_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(CreateCorrectCar_When_ParamsAreValid));
            manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            carModel = new CarModel { Id = 1, ManufacturerId = manufacturer.Id, Name = "Civik" };
            user = new User { Id = 1, Email = "petur@slabeev.com", UserName = "petur", PhoneNumber = "0979146521" };
            carDTO = new CarDTO { Id = 1, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB2342AS", VIN = "23423423234234234", IsDeleted = null };
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarService(assertContext, mapper);
                assertContext.Manufacturers.Add(manufacturer);
                assertContext.CarModels.Add(carModel);
                assertContext.Users.Add(user);
                assertContext.SaveChanges();

                var result = await sut.CreateCarAsync(carDTO);

                Assert.AreEqual(carDTO.Id, result.Id);
                Assert.AreEqual(carDTO.VIN, result.VIN);
                Assert.AreEqual(carDTO.CarModelId, result.CarModelId);
                Assert.AreEqual(carDTO.RegistrationPlate, result.RegistrationPlate);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_CarAlreadyExists()
        {
            options = Utils.GetOptions(nameof(ThrowExceptionAndCorrectMessage_When_CarAlreadyExists));

            manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            carModel = new CarModel { Id = 1, ManufacturerId = manufacturer.Id, Name = "Civik" };
            user = new User { Id = 1, Email = "petur@slabeev.com", UserName = "petur", PhoneNumber = "0979146521" };
            carDTO = new CarDTO { Id = 1, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB2342AS", VIN = "23423423234234234", IsDeleted = null };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                var sut = new CarService(arrangeContext, mapper);
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();

                var result = await sut.CreateCarAsync(carDTO);
            }

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new CarService(assertContext, mapper);
                string expectedExceptionMessage = $"Car with VIN: {carDTO.VIN} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(() => sut.CreateCarAsync(carDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }
    }
}
