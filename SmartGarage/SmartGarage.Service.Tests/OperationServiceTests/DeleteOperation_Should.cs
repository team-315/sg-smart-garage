﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.OperationServiceTests
{
    [TestClass]
    public class DeleteOperation_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Operation operation;
        private Operation operationDel;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            options = Utils.GetOptions(nameof(TestContext.TestName));

            operation = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            operationDel = new Operation { Id = 2, Name = "Oil Deleted", PriceValue = 69, IsDeleted = DateTime.Now };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Operations.Add(operation);
                arrangeContext.Operations.Add(operationDel);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationNotFound()
        {
            // Act & Assert
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                const int testId = int.MaxValue;
                string expectedExceptionMessage = $"Operation does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.DeleteAsync(testId));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationIsAlreadyDeleted()
        {
            // Act & Assert
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                string expectedExceptionMessage = $"Operation does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.DeleteAsync(operationDel.Id));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task DeleteWithCorrectParameters()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                await sut.DeleteAsync(operation.Id);

                var result = assertContext.Operations.Find(1);

                Assert.AreEqual(false, result.IsDeleted == null);
            }
        }
    }
}
