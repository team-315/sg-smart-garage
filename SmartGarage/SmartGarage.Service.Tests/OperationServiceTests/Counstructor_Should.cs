﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Service.Tests.OperationServiceTests
{
    [TestClass]
    public class Counstructor_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public void CreateInstance()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new SmartGarageContext(options);

            var sut = new OperationService(context, mapper);

            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenAnyArgumentIsNull()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new SmartGarageContext(options);

            Assert.ThrowsException<ArgumentNullException>
                (() => new OperationService(context, null));

            Assert.ThrowsException<ArgumentNullException>
                (() => new OperationService(null, mapper));
        }
    }
}
