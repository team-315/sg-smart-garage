﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.OperationServiceTests
{
    [TestClass]
    public class CreateOperation_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationAlreadyExists()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationDTO = new OperationDTO { Id = 1, Name = "Oil Change", PriceValue = 100, IsDeleted = null };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(arrangeContext, mapper);
                var result = await sut.CreateAsync(operationDTO);
            }

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                string expectedExceptionMessage = $"Operation {operationDTO.Name} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.CreateAsync(operationDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task CreateCorrectOperation_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationDTO = new OperationDTO { Id = 1, Name = "Oil Change", PriceValue = 100, IsDeleted = null };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                var result = await sut.CreateAsync(operationDTO);

                Assert.AreEqual(operationDTO.Name, result.Name);
                Assert.AreEqual(operationDTO.PriceValue, result.PriceValue);
            }
        }
    }
}
