﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.OperationServiceTests
{
    [TestClass]
    public class GetAllOperations_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ReturnEmpty_When_NotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertcontext, mapper);
                var result = await sut.GetAllOperationsAsync();

                Assert.IsFalse(result.Any());
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollection()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            var operationB = new Operation { Id = 2, Name = "Filters Change", PriceValue = 111, IsDeleted = null }; ;

            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Operations.Add(operationA);
                arrangecontext.Operations.Add(operationB);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertcontext, mapper);
                var result = await sut.GetAllOperationsAsync();

                Assert.IsInstanceOfType(result, typeof(List<OperationDTO>));
                Assert.AreEqual(2, result.Count);

                Assert.AreEqual(operationA.Id, result.First().Id);
                Assert.AreEqual(operationA.Name, result.First().Name);
                Assert.AreEqual(operationB.Id, result.ToList()[1].Id);
                Assert.AreEqual(operationB.Name, result.ToList()[1].Name);
            }
        }

        [TestMethod]
        public async Task DoNotReturn_DeletedOperations()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            var operationB = new Operation { Id = 2, Name = "Filters Change", PriceValue = 111, IsDeleted = null }; 
            var operationC = new Operation { Id = 3, Name = "Scam", PriceValue = 1500, IsDeleted = DateTime.Now }; 

            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Operations.Add(operationA);
                arrangecontext.Operations.Add(operationB);
                arrangecontext.Operations.Add(operationC);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertcontext, mapper);
                var result = await sut.GetAllOperationsAsync();

                Assert.IsInstanceOfType(result, typeof(List<OperationDTO>));
                Assert.AreEqual(2, result.Count);

                Assert.AreEqual(operationA.Id, result.First().Id);
                Assert.AreEqual(operationA.Name, result.First().Name);
                Assert.AreEqual(operationB.Id, result.ToList()[1].Id);
                Assert.AreEqual(operationB.Name, result.ToList()[1].Name);
            }
        }
    }
}
