﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.OperationServiceTests
{
    [TestClass]
    public class GetOperation_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_WhenOperationIsNotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertcontext = new SmartGarageContext(options))
            {

                var sut = new OperationService(assertcontext, mapper);
                const int invalidId = -1;
                string expectedExceptionMessage = "Operation does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetOperationAsync(invalidId));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_WhenOperationIsDeleted()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operation = new Operation { Id = 1, Name = "Oil Change", PriceValue = 100, IsDeleted = DateTime.Now };
            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Operations.Add(operation);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertcontext, mapper);
                //var result = await sut.GetOperationAsync(operation.Id);
                string expectedExceptionMessage = "Operation does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetOperationAsync(operation.Id));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectOperationDTO_WhenParametersAreCorrect()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operation = new Operation { Id = 1, Name = "Oil Change", PriceValue = 100, IsDeleted = null };
            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Operations.Add(operation);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertcontext, mapper);
                var result = await sut.GetOperationAsync(operation.Id);

                Assert.AreEqual(operation.Id, result.Id);
                Assert.AreEqual(operation.Name, result.Name);
                Assert.AreEqual(operation.PriceValue, result.PriceValue);
            }
        }
    }
}
