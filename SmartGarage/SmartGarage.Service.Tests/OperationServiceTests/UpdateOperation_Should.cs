﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.OperationServiceTests
{
    [TestClass]
    public class UpdateOperation_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Operation operationA;
        private Operation operationB;
        private Operation operationDel;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            options = Utils.GetOptions(nameof(TestContext.TestName));

            operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            operationDel = new Operation { Id = 2, Name = "Oil Deleted", PriceValue = 69, IsDeleted = DateTime.Now };
            operationB = new Operation { Id = 3, Name = "Filter Change", PriceValue = 60, IsDeleted = null };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Operations.Add(operationA);
                arrangeContext.Operations.Add(operationB);
                arrangeContext.Operations.Add(operationDel);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationDoesNotExist()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationDTO = new OperationDTO { Id = -1, Name = "Oil Change", PriceValue = 100, IsDeleted = null };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                string expectedExceptionMessage = $"Operation does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.UpdateAsync(operationDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationIsDeleted()
        {
            var operationDTO = new OperationDTO { Id = operationDel.Id, Name = "test", PriceValue = 666, IsDeleted = null };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                string expectedExceptionMessage = $"Operation does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.UpdateAsync(operationDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationPriceIsNegative()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationDTO = new OperationDTO { Id = 1, Name = "test", PriceValue = -100, IsDeleted = null };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new OperationService(assertContext, mapper);
                string expectedExceptionMessage = "Price cannot be below 0";

                InvalidParameterException realException = await Assert
                    .ThrowsExceptionAsync<InvalidParameterException>(
                    () => sut.UpdateAsync(operationDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_OperationNameAlreadyExist()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var operationDTO = new OperationDTO { Id = operationA.Id, Name = operationB.Name, PriceValue = 150, IsDeleted = null };

                var sut = new OperationService(assertContext, mapper);
                string expectedExceptionMessage = $"Operation {operationDTO.Name} already exists";

                ResourceAlreadyExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceAlreadyExistException>(
                    () => sut.UpdateAsync(operationDTO));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task UpdateOperation_When_ParamsAreValid()
        {
            using (var assertContext = new SmartGarageContext(options))
            {
                var operationToUpdate = new OperationDTO { Id = 1, Name = "Oil Change", PriceValue = 150};

                var sut = new OperationService(assertContext, mapper);
                await sut.UpdateAsync(operationToUpdate);

                var updatedOperation = assertContext.Operations.Find(operationToUpdate.Id);

                Assert.AreEqual(operationToUpdate.Name,updatedOperation.Name);
                Assert.AreEqual(operationToUpdate.PriceValue,updatedOperation.PriceValue);
            }
        }
    }
}
