﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Service.Tests.CurrencyConverterTests
{
    [TestClass]
    public class GetCurrency_Should : BaseTest
    {
        private readonly static CurrencyConverterService sut = new CurrencyConverterService();

        [TestMethod]
        public void ConvertCurrencyWithRightParameters_Should()
        {
            var result = sut.GetCurrency("USD");


            Assert.AreEqual("USD", result.Rates.Keys.First());
        }
    }
}
