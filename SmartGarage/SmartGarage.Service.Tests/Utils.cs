﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data.Context;

namespace SmartGarage.Service.Tests
{
    public class Utils
    {
        public static DbContextOptions<SmartGarageContext> GetOptions(string databaseName)
        {
            var options = new DbContextOptionsBuilder<SmartGarageContext>()
                              .UseInMemoryDatabase(databaseName)
                              .Options;

            return options;
        }
    }
}
