﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Mapper;
using SmartGarage.Services.Settings;

namespace SmartGarage.Service.Tests
{
    public abstract class BaseTest
    {
        protected DTOMapper mapper;

        public BaseTest()
        {
            mapper = new DTOMapper();
        }

        [TestCleanup()]
        public void Cleanup()
        {
            var context = new SmartGarageContext(Utils.GetOptions(nameof(TestContext.TestName)));

            context.Database.EnsureDeleted();
        }
    }
}
