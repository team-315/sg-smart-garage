﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.VisitServiceTests
{
    [TestClass]
    public class CreateVisit_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Operation operationA;
        private Operation operationB;
        private Manufacturer manufacturer;
        private CarModel carModel;
        private Car car;
        private User user;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            options = Utils.GetOptions(nameof(TestContext.TestName));
            operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            operationB = new Operation { Id = 2, Name = "Filter Change", PriceValue = 60, IsDeleted = null };
            manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            carModel = new CarModel { Id = 1, ManufacturerId = manufacturer.Id, Name = "Civik" };
            user = new User { Id = 1, Email = "petur@slabeev.com", UserName = "petur", PhoneNumber = "0979146521" };
            car = new Car { Id = 1, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB2342AS", VIN = "234234", IsDeleted = null };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Operations.Add(operationA);
                arrangeContext.Operations.Add(operationB);
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.Users.Add(user);
                arrangeContext.Cars.Add(car);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task CreateCorrectVisit_When_ParamsAreValid()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var carId = car.Id;
            var operationIds = new List<int> { operationA.Id, operationB.Id };

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertContext, mapper);
                var visitId = await sut.CreateAsync(carId, operationIds);

                //TODO: ne sum re6il kak da vru6tam cqloto DTO , (predpolagam 4e) tova ne e unit Test!
                var result = await sut.GetVisitAsync(visitId);
                List<int> resultOperations = result.Operations.Select(o => o.Id).ToList();

                Assert.AreEqual(carId, result.CarId);
                Assert.AreEqual(visitId, result.Id);
                Assert.IsTrue(resultOperations.Contains(operationA.Id));
                Assert.IsTrue(resultOperations.Contains(operationB.Id));
            }
        }
    }
}
