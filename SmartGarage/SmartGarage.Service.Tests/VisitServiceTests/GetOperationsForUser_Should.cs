﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.VisitServiceTests
{
    [TestClass]
    public class GetOperationsForUser_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Operation operationA;
        private Operation operationB;
        private OperationVisit operationVisitA;
        private OperationVisit operationVisitB;
        private Manufacturer manufacturer;
        private CarModel carModel;
        private Car carA;
        private Car carB;
        private User user;
        private Visit visit;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            options = Utils.GetOptions(nameof(TestContext.TestName));
            operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            operationB = new Operation { Id = 2, Name = "Filter Change", PriceValue = 60, IsDeleted = null };
            operationVisitA = new OperationVisit { VisitId = 1, OperationId = 1 };
            operationVisitB = new OperationVisit { VisitId = 1, OperationId = 2 };
            
            manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            carModel = new CarModel { Id = 1, ManufacturerId = manufacturer.Id, Name = "Civik" };
            user = new User { Id = 1, Email = "petur@slabeev.com", UserName = "petur", PhoneNumber = "0898743412" };
            carA = new Car { Id = 1, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB2342AS", VIN = "234234", IsDeleted = null };
            carB = new Car { Id = 2, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB9463PA", VIN = "634564", IsDeleted = null };
            

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Operations.Add(operationA);
                arrangeContext.Operations.Add(operationB);
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.Users.Add(user);
                arrangeContext.Cars.Add(carA);
                arrangeContext.Cars.Add(carB);
                
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ReturnEmpty_When_NotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertcontext, mapper);
                var result = await sut.GetAllOperationsFiltered(user.Id);

                Assert.IsFalse(result.Any());
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollection()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            visit = new Visit { Id = 1, Date = DateTime.Today, CarId = carA.Id };
            var listOV = new List<OperationVisit> { operationVisitA, operationVisitB };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Visits.Add(visit);
                arrangeContext.AddRange(listOV);
                arrangeContext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertcontext, mapper);
                

                var result = await sut.GetAllOperationsFiltered(user.Id);

                Assert.IsInstanceOfType(result, typeof(List<VisitDTO>));
                Assert.AreEqual(1, result.Count());
                Assert.AreEqual(operationVisitA.OperationId, result.First().Id);
                Assert.AreEqual(operationA.Id, result.First().Id);
            }
        }
    }
}
