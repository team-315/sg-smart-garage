﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Service.Tests.VisitServiceTests
{
    [TestClass]
    public class Constructor_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public void CreateInstance()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new SmartGarageContext(options);

            var sut = new VisitService(context, mapper);

            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void ThrowArgumentNullException_WhenAnyArgumentIsNull()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var context = new SmartGarageContext(options);

            Assert.ThrowsException<ArgumentNullException>
                (() => new VisitService(context, null));

            Assert.ThrowsException<ArgumentNullException>
                (() => new VisitService(null, mapper));
        }
    }
}
