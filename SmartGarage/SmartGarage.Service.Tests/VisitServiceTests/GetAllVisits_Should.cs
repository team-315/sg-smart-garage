﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.VisitServiceTests
{
    [TestClass]
    public class GetAllVisits_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ReturnEmpty_When_NotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertcontext, mapper);
                var result = await sut.GetAllVisitsAsync();

                Assert.IsFalse(result.Any());
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollection()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 100, IsDeleted = null };
            var operationB = new Operation { Id = 2, Name = "Filter Change", PriceValue = 100, IsDeleted = null };
            var operationC = new Operation { Id = 3, Name = "Else Change", PriceValue = 100, IsDeleted = null };
            var list = (new List<Operation> { operationA, operationB, operationC });
            var manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            var carModel = new CarModel { Id = 1, Manufacturer = manufacturer, Name = "Civik" };
            var user = new User { Id = 1, UserName = "test", Email = "Test@email.com", PhoneNumber = "0979141193" };
            var car = new Car { Id = 1, User = user, CarModel = carModel, RegistrationPlate = "RB2342AS", VIN = "253412331534543534" };
            var visitA = new Visit { Id = 1, Date = DateTime.Today, Car = car };
            var visitB = new Visit { Id = 2, Date = DateTime.Now, Car = car };

            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Manufacturers.Add(manufacturer);
                arrangecontext.CarModels.Add(carModel);
                arrangecontext.Users.Add(user);
                arrangecontext.Operations.AddRange(list);
                arrangecontext.Visits.Add(visitA);
                arrangecontext.Visits.Add(visitB);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertcontext, mapper);

                var result = await sut.GetAllVisitsAsync();

                Assert.IsInstanceOfType(result, typeof(List<VisitDTO>));
                Assert.AreEqual(2, result.Count);

                Assert.AreEqual(visitA.Id, result.First().Id);
                Assert.AreEqual(visitB.Id, result.ToList()[1].Id);
            }
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                           

                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                