﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.VisitServiceTests
{
    [TestClass]
    public class UpdateVisitCar_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;
        private Operation operationA;
        private Operation operationB;
        private OperationVisit operationVisitA;
        private OperationVisit operationVisitB;
        private Manufacturer manufacturer;
        private CarModel carModel;
        private Car carA;
        private Car carB;
        private User user;
        private Visit visit;

        [TestInitialize()]
        public void Initialize()
        {
            //Arrange
            options = Utils.GetOptions(nameof(TestContext.TestName));
            operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 60, IsDeleted = null };
            operationB = new Operation { Id = 2, Name = "Filter Change", PriceValue = 60, IsDeleted = null };
            operationVisitA = new OperationVisit { VisitId = 1, OperationId = 1 };
            operationVisitB = new OperationVisit { VisitId = 1, OperationId = 2 };
            var listOV = new List<OperationVisit> { operationVisitA, operationVisitB };
            manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            carModel = new CarModel { Id = 1, ManufacturerId = manufacturer.Id, Name = "Civik" };
            user = new User { Id = 1, Email = "petur@slabeev.com", UserName = "petur", PhoneNumber = "0898743412" };
            carA = new Car { Id = 1, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB2342AS", VIN = "234234", IsDeleted = null };
            carB = new Car { Id = 2, UserId = user.Id, CarModelId = carModel.Id, RegistrationPlate = "RB9463PA", VIN = "634564", IsDeleted = null };
            visit = new Visit { Id = 1, Date = DateTime.Today, CarId = carA.Id};

            using (var arrangeContext = new SmartGarageContext(options))
            {
                arrangeContext.Operations.Add(operationA);
                arrangeContext.Operations.Add(operationB);
                arrangeContext.Manufacturers.Add(manufacturer);
                arrangeContext.CarModels.Add(carModel);
                arrangeContext.Users.Add(user);
                arrangeContext.Cars.Add(carA);
                arrangeContext.Cars.Add(carB);
                arrangeContext.Visits.Add(visit);
                arrangeContext.AddRange(listOV);
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_When_VisitDoesNotExist()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            const int nonExistingId = -1;

            var visitId = nonExistingId;
            var carToUpdateId = carB.Id;

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertContext, mapper);
                string expectedExceptionMessage = $"Visit does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.UpdateCarForVisitAsync(visitId, carToUpdateId));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task UpdateVisitCar_When_ParamsAreCorrect()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            var visitId = visit.Id;
            var carToUpdateId = carB.Id;

            using (var assertContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertContext, mapper);
                await sut.UpdateCarForVisitAsync(visitId, carToUpdateId);

                var updatedVisit = assertContext.Visits.Find(visitId);

                Assert.AreEqual(carToUpdateId, updatedVisit.CarId);

            
            }
        }
    }
}
