﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data.Context;
using SmartGarage.Data.Models;
using SmartGarage.Services.Exceptions;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Service.Tests.VisitServiceTests
{
    [TestClass]
    public class GetVisit_Should : BaseTest
    {
        private DbContextOptions<SmartGarageContext> options;

        [TestMethod]
        public async Task ThrowExceptionAndCorrectMessage_WhenVisitIsNotFound()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var assertcontext = new SmartGarageContext(options))
            {

                var sut = new VisitService(assertcontext, mapper);
                const int invalidId = -1;
                string expectedExceptionMessage = "Visit does not exist";

                ResourceNotExistException realException = await Assert
                    .ThrowsExceptionAsync<ResourceNotExistException>(
                    () => sut.GetVisitAsync(invalidId));

                Assert.AreEqual(expectedExceptionMessage, realException.Message);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectOperationDTO_WhenParametersAreCorrect()
        {
            options = Utils.GetOptions(nameof(TestContext.TestName));
            var operationA = new Operation { Id = 1, Name = "Oil Change", PriceValue = 100, IsDeleted = null };
            var operationB = new Operation { Id = 2, Name = "Filter Change", PriceValue = 100, IsDeleted = null };
            var operationC = new Operation { Id = 3, Name = "Else Change", PriceValue = 100, IsDeleted = null };
            var operationVisitA = new OperationVisit { VisitId = 1, OperationId = 1};
            var operationVisitB = new OperationVisit { VisitId = 1, OperationId = 2};
            var operationVisitC = new OperationVisit { VisitId = 1, OperationId = 3};
            //var list = new List<Operation> { operationA, operationB, operationC };
            var listOV = new List<OperationVisit> { operationVisitA, operationVisitB, operationVisitC };

            var manufacturer = new Manufacturer { Id = 1, Name = "Honda" };
            var carModel = new CarModel { Id = 1, Manufacturer = manufacturer, Name = "Civik" };
            var user = new User { Id = 1, UserName = "test", Email = "Test@email.com", PhoneNumber = "0979141193" };
            var car = new Car { Id = 1, User = user, CarModel = carModel, RegistrationPlate = "RB2342AS", VIN = "253412331534543534" };
            var visit = new Visit { Id = 1, Date = DateTime.Today, CarId = car.Id };

            using (var arrangecontext = new SmartGarageContext(options))
            {
                arrangecontext.Cars.Add(car);
                arrangecontext.Visits.Add(visit);
                //arrangecontext.OperationVisits.AddRange():

                arrangecontext.AddRange(listOV);
                arrangecontext.SaveChanges();
            }

            using (var assertcontext = new SmartGarageContext(options))
            {
                var sut = new VisitService(assertcontext, mapper);
                var result = await sut.GetVisitAsync(visit.Id);

                Assert.AreEqual(visit.Id, result.Id);
                Assert.AreEqual(visit.Car.Id, car.Id);
                var allOperationIds = visit.Visits.Select(ov => ov.OperationId).ToList();
                // Assert. operation expected .count , result.oper.count
                Assert.IsTrue(allOperationIds.Contains(operationA.Id));
                Assert.IsTrue(allOperationIds.Contains(operationB.Id));
                Assert.IsTrue(allOperationIds.Contains(operationC.Id));
            }
        }
    }
}
