﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartGarage.Web.ViewModels.Email
{
    public class WelcomeRequest
    {
        public string ToEmail { get; set; }
        public string UserName { get; set; }
    }
}
