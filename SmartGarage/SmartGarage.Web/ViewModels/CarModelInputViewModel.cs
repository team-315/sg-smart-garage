﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SmartGarage.Web.ViewModels
{
    public class CarModelInputViewModel
    {
        public string Name { get; set; }
        public string ManufacturerName { get; set; }
    }
}
