﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ViewModels
{
    public class OperationInputViewModel
    {
        public string Name { get; set; }
        public double PriceValue { get; set; }
    }
}
