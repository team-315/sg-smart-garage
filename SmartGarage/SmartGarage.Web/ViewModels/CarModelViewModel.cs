﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SmartGarage.Web.ViewModels
{
    public class CarModelViewModel
    {
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        [JsonIgnore]
        public ManufacturerDTO Manufacturer { get; set; }
    }
}
