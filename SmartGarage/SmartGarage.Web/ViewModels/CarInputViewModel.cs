﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ViewModels
{
    public class CarInputViewModel
    {
        public int UserId { get; set; }
        public string CarModelName { get; set; }
        public string ManufacturerName { get; set; }
        public string VIN { get; set; }
        public string RegistrationPlate { get; set; }

    }
}
