﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using SmartGarage.Web.Mapper;
using SmartGarage.Web.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiController
{
    [Route("api/operation")]
    [ApiController]
    public class OperationApiController : ControllerBase
    {

        private readonly IOperationService operationService;
        private readonly ViewModelMapper mapper;

        public OperationApiController(IOperationService operationService, ViewModelMapper mapper)
        {
            this.operationService = operationService;
            this.mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetOperation(int id)
        {
            try
            {
                var operationDTO = await operationService.GetOperationAsync(id);

                var operationViewModel = mapper.OperationDTOToVM(operationDTO);

                return Ok(operationViewModel);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }

        [HttpGet("")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllOperations()
        {
            var operationDTO = await operationService.GetAllOperationsAsync();

            var operationViewModels = operationDTO.Select(x => mapper.OperationDTOToVM(x));

            return Ok(operationViewModels);
        }

        [HttpGet("name/price")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllOperationsByNameAndPrice([FromHeader] string name, [FromHeader] int? price)
        {
            var operationDTO = await operationService.GetAllOperationsByNameAndPriceAsync(name, price);

            var operationViewModels = operationDTO.Select(x => mapper.OperationDTOToVM(x));

            return Ok(operationViewModels);
        }

        [HttpPost("")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> Create([FromBody] OperationInputViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var operationDTO = mapper.OperationInputVMToDTO(model);

            try
            {
                var newOperation = await this.operationService.CreateAsync(operationDTO);

                model = mapper.OperationDTOToInputVM(newOperation);

                return Created("CreatedOperation", model);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourceAlreadyExistException e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> Update(int id, [FromBody] OperationInputViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var operationDTO = mapper.OperationInputVMToDTO(model);
            operationDTO.Id = id;

            try
            {
                await this.operationService.UpdateAsync(operationDTO);

                return NoContent();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourceAlreadyExistException e)
            {
                return Conflict(e.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await operationService.DeleteAsync(id);
                return NoContent();
            }
            catch(ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }
    }
}
