﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Web.ViewModels.Email;
using System;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailApiController : ControllerBase
    {
        private readonly IMailService mailService;

        public MailApiController(IMailService mailService)
        {
            this.mailService = mailService;
        }

        [HttpPost("send")]
        public async Task<IActionResult> SendMail([FromForm] MailRequest request)
        {
            try
            {
                //await mailService.SendEmailAsync(request);
                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
