﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Exceptions;
using SmartGarage.Web.Mapper;
using SmartGarage.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarApiController : ControllerBase
    {
        private readonly ICarService carService;
        private readonly ICarModelService carModelService;
        private readonly IManufacturerService manufacturerService;
        private readonly IUserService userService;
        private readonly ViewModelMapper mapper;

        public CarApiController(ICarService carService, ViewModelMapper mapper, 
                                ICarModelService carModelService, 
                                IManufacturerService manufacturerService,
                                IUserService userService)
        {
            this.carService = carService;
            this.carModelService = carModelService;
            this.manufacturerService = manufacturerService;
            this.userService = userService;
            this.mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetCar(int id)
        {
            try
            {
                var carDTO = await this.carService.GetCarAsync(id);
                var carViewModel = mapper.CarDTOToVM(carDTO);
                return Ok(carViewModel);
            }
            catch(ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllCars()
        {
            try
            {
                var carDTOs = await this.carService.GetAllCarsAsync();
                
                var carViewModels = carDTOs.Select(c => mapper.CarDTOToVM(c));

                return Ok(carViewModels);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("user/{id}")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> ListCarsByCustomerId(int id)
        {
            try
            {
                var userId = await userService.ValidateUserExist(id);

                var carDTO = await carService.GetAllCarsForUser(id);

                return Ok(carDTO);
            }
            catch (InvalidParameterException e)
            {
                return NotFound(e.Message);
            }
            catch(ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            
        }

        [HttpPost("")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> CreateCar([FromBody] CarInputViewModel model)
        {
            try
            {
                var carDTO = mapper.CarInputVMToDTO(model);
                
                var userId = await userService.ValidateUserExist(model.UserId);

                var manufacturerId = await manufacturerService.CreateIfNotExistAsync(model.ManufacturerName);

                var carModelId = await carModelService.CreateCarModelIfNotExistAsync(model.CarModelName, manufacturerId);

                carDTO.UserId = userId;
                carDTO.CarModelId = carModelId;
               
                var newCar = await this.carService.CreateCarAsync(carDTO);

                return Ok($"Created new car for customer with ID: {model.UserId}");
            }
            catch (ResourceAlreadyExistException e)
            {
                return Conflict(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> UpdateCar(int id, [FromBody] CarInputViewModel car)
        {
            try
            {
                var carDTO = mapper.CarInputVMToDTO(car);
                carDTO.Id = id;

                var manufacturerId = await manufacturerService.CreateIfNotExistAsync(car.ManufacturerName);

                var carModelId = await carModelService.CreateCarModelIfNotExistAsync(car.CarModelName, manufacturerId);
                carDTO.CarModelId = carModelId;

                await this.carService.UpdateCarAsync(carDTO);

                return Ok("Car was updated successfully!");
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
