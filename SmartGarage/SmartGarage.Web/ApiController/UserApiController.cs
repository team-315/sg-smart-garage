﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Exceptions;
using SmartGarage.Web.Mapper;
using SmartGarage.Web.ViewModels;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiController
{
    [Route("api/user")]
    [ApiController]
    public class UserApiController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IVisitService visitService;
        private readonly IMailService mailService;
        private readonly ViewModelMapper mapper;

        public UserApiController(IUserService userService, ViewModelMapper mapper, IMailService mailService, IVisitService visitService)
        {
            this.userService = userService;
            this.mailService = mailService;
            this.visitService = visitService;
            this.mapper = mapper;
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                var userDTO = await userService.GetUserAsync(id);

                return Ok(userDTO);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllUsers()
        {
            var userDTOs = await userService.GetAllUsersAsync();

            return Ok(userDTOs);
        }

        [HttpGet("customers")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllCustomers()
        {
            var userDTOs = await userService.GetAllCustomersAsync();

            return Ok(userDTOs);
        }

        [HttpGet("sorted/name")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllCustomersByName()
        {
            var userDTOs = await userService.GetAllCustomersSortedByNameAsync();

            return Ok(userDTOs);
        }

        [HttpGet("sorted/visitDate")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllUsersSortedByVisitsAsync()
        {
            var intIds = await visitService.GetAllUserIdsOrderedByVisitDate();

            var userDTOs = await userService.GetAllCustomersSortedByVisitsAsync(intIds);

            return Ok(userDTOs);
        }

        [HttpPost("filtered")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> GetAllCustomersFilteredAsync(CustomerFilterModel model)
        {
            var filterDTO = mapper.CustomerFilterModelToDTO(model);

            var userDTOs = await userService.GetAllCustomersFilteredAsync(filterDTO);

            return Ok(userDTOs);
        }

        [HttpPost("register")]
        public async Task<ActionResult> RegisterAsync(UserInputViewModel model)
        {
            var userDTO = this.mapper.UserInputVMToDTO(model);
            try
            {
                var result = await userService.RegisterAsync(userDTO, model.Password);

                await mailService.SendWelcomeEmailAsync(userDTO.UserName + " " + userDTO.LastName, userDTO.Email);

                return Ok(result);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (ResourceAlreadyExistException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }

        [HttpPost("token")]
        public async Task<IActionResult> GetTokenAsync([FromHeader] string email, [FromHeader] string password)
        {
            var result = await userService.GetTokenAsync(email, password);

            return Ok(result);
        }

        [HttpPost("assignrole")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> AssignRoleAsync(AssignRoleViewModel model)
        {
            var assignRoleDTO = mapper.AssignRoleInputVMToDTO(model);

            try
            {
                var result = await userService.AssignRoleAsync(assignRoleDTO);

                return Ok(result);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }

        [Authorize]
        [HttpPut("newpass/{newPassword}")]
        public async Task<IActionResult> ChangePasswordAsync(string newPassword)
        {
            var authenticatedUserEmail = this.User.FindFirstValue(ClaimTypes.Email);
            try
            {
                await userService.ChangePasswordAsync(authenticatedUserEmail, newPassword);

                return Ok("Successful change!");
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong.");
            }
        }

        [HttpPut("forgotpass/{newPassword}")]
        public async Task<IActionResult> ChangePasswordAsync(string newPassword, string email)
        {
            
            try
            {
                await userService.ChangePasswordAsync(email, newPassword);

                return Ok("Successful change!");
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }

        [HttpDelete("")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await userService.DeleteAsync(id);
                return NoContent();
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
