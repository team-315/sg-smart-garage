﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Exceptions;
using SmartGarage.Web.Mapper;
using SmartGarage.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarModelApiController : ControllerBase
    {
        private readonly ICarModelService carModelService;
        private readonly IManufacturerService manufacturerService;
        private readonly ViewModelMapper mapper;

        public CarModelApiController(ICarModelService carModelService, IManufacturerService manufacturerService ,ViewModelMapper mapper)
        {
            this.carModelService = carModelService;
            this.manufacturerService = manufacturerService;
            this.mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCarModel(int id)
        {
            try
            {
                var carModelDTO = await this.carModelService.GetCarModelAsync(id);
                var carModel = mapper.CarModelDTOToVM(carModelDTO);
                return Ok(carModel);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllCarModels()
        {
            try
            {
                var carModelDTOs = await this.carModelService.GetAllCarModelsAsync();

                var carModelViewModels = carModelDTOs.Select(c => mapper.CarModelDTOToVM(c));

                return Ok(carModelViewModels);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            
        }
    }
}
