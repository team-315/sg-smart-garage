﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;



namespace SmartGarage.Web.ApiController
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SecuredController : ControllerBase
    {
        [HttpGet("")]
        public async Task<IActionResult> GetSecuredData()
        {
            return Ok("WE HAVE AUTHENTICATED SUCCESSFULLY !This Secured Data is available only for Authenticated Users.");
        }

        [HttpPost("")]
        [Authorize(Roles = "Owner")]
        public async Task<IActionResult> PostSecuredData()
        {
            var userEmail = this.User.FindFirstValue(ClaimTypes.Email);
            return Ok("WE HAVE AUTHORIZED SUCCESSFULLY !This Secured Data is available only for Authenticated Users.");
        }
    }
}
