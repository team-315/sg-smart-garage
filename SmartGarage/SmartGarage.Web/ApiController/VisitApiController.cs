﻿using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Exceptions;
using SmartGarage.Web.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using SmartGarage.Web.ViewModels;

namespace SmartGarage.Web.ApiController
{
    [Route("api/visit")]
    [ApiController]
    public class VisitApiController : ControllerBase
    {
        private readonly IVisitService visitService;
        private readonly IUserService userService;
        private readonly ICarService carService;
        private readonly IOperationService operationService;
        private readonly ICurrencyConverterService currencyConverterService;
        private readonly ViewModelMapper mapper;

        public VisitApiController(IVisitService visitService, ViewModelMapper mapper, IUserService userService, ICarService carService,
            IOperationService operationService, ICurrencyConverterService currencyConverterService)
        {
            this.visitService = visitService;
            this.mapper = mapper;
            this.userService = userService;
            this.carService = carService;
            this.operationService = operationService;
            this.currencyConverterService = currencyConverterService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetVisit(int id, string currency = "EUR")
        {
            try
            {
                var visitDTO = await visitService.GetVisitAsync(id);

                if (currency != "EUR")
                {
                    var rate = currencyConverterService.GetCurrency(currency);
                    
                    if (rate.Success)
                    {
                        var visitcurrencyVM = mapper.VisitDTOToViewModel(visitDTO, currency, rate.Rates.Values.First());

                        return Ok(visitcurrencyVM);
                    }
                    else
                    {
                        return BadRequest("Something went wrong");
                    }
                }

                var visitVM = mapper.VisitDTOToViewModel(visitDTO);

                return Ok(visitVM);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong");
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllVisits()
        {
            var visitDTO = await visitService.GetAllVisitsAsync();



            var visitVMs = visitDTO.Select(v => mapper.VisitDTOToViewModel(v)).ToList();

            return Ok(visitVMs);
        }

        [Authorize]
        [HttpPost("operatioins/customer")]
        public async Task<IActionResult> GetAllOperationsFilteredByCustomerDate(OperationsFilterModel model, string currency = "EUR")
        {
            var authenticatedUserEmail = this.User.FindFirstValue(ClaimTypes.Email);

            int id = userService.TryGetUserId(authenticatedUserEmail);

            var filterDTO = mapper.OperationsFilterModelToDTO(model);

            var visitDTO = await visitService.GetAllOperationsFiltered(id, filterDTO);

            if (currency != "EUR")
            {
                var rate = currencyConverterService.GetCurrency(currency);
                
                if (rate.Success)
                {
                    var visitcurrencyVMs = visitDTO.Select(v => mapper.VisitDTOToViewModel(v, currency, rate.Rates.Values.First())).ToList();

                    return Ok(visitcurrencyVMs);
                }
                else
                {
                    return BadRequest("Something went wrong");
                }
            }
            
            var visitVMs = visitDTO.Select(v => mapper.VisitDTOToViewModel(v)).ToList();
            return Ok(visitVMs);
        }

        [HttpPost("")]
        [Authorize(Roles = "Employee")]
        public async Task<IActionResult> Create([FromHeader] int carId, [FromHeader] List<int> operationIds)
        {
            try
            {
                operationService.ValidateOperationsExist(operationIds);

                carService.ValidateCarExists(carId);

                var createdVisitId = await visitService.CreateAsync(carId, operationIds);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong, try again later.");
            }

            return Ok("Visit created");
        }

        [HttpPut("/car")]
        public async Task<IActionResult> UpdateCarForVisit([FromHeader] int id, [FromHeader] int carId)
        {
            try
            {
                carService.ValidateCarExists(carId);

                await this.visitService.UpdateCarForVisitAsync(id, carId);

                return Ok();
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong, try again later.");
            }
        }
    }
}
