﻿using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Exceptions;
using SmartGarage.Web.Mapper;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.ApiController
{
    [Route("api/manufacturer")]
    [ApiController]
    public class ManufacturerApiController : ControllerBase
    {
        private readonly IManufacturerService manufacturerService;
        private readonly ViewModelMapper mapper;

        public ManufacturerApiController(IManufacturerService manufacturerService, ViewModelMapper mapper)
        {
            this.manufacturerService = manufacturerService;
            this.mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetManufacturer(int id)
        {
            try
            {
                var manufacturerDTO = await manufacturerService.GetManufacturerAsync(id);

                var manufacturerViewModel = mapper.ManufacturerDTOToVM(manufacturerDTO);

                return Ok(manufacturerViewModel);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAllManufacturers()
        {
            try
            {
                var manufacturerDTOs = await manufacturerService.GetAllManufacturersAsync();

                var manufacturerViewModels = manufacturerDTOs.Select(x => mapper.ManufacturerDTOToVM(x));

                return Ok(manufacturerViewModels);
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
