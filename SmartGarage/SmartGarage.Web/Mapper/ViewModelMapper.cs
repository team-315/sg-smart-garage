﻿using SmartGarage.Services.DTOs;
using SmartGarage.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartGarage.Web.Mapper
{
    public class ViewModelMapper
    {
        public ManufacturerViewModel ManufacturerDTOToVM(ManufacturerDTO model)
        {
            var manufacturerVM = new ManufacturerViewModel();

            manufacturerVM.Id = model.Id;
            manufacturerVM.Name = model.Name;

            return manufacturerVM;
        }

        public ManufacturerDTO ManufacturerVMToDTO(ManufacturerViewModel model)
        {
            var manufacturerDTO = new ManufacturerDTO();

            manufacturerDTO.Id = model.Id;
            manufacturerDTO.Name = model.Name;

            return manufacturerDTO;
        }

        public OperationViewModel OperationDTOToVM(OperationDTO model, string currency = "EUR", double rate = 1)
        {
            var operationVM = new OperationViewModel();

            operationVM.Id = model.Id;
            operationVM.Name = model.Name;
            operationVM.PriceValue = model.PriceValue*rate;
            operationVM.PriceValue = Math.Round(operationVM.PriceValue, 2);
            operationVM.PriceCurrency = currency;

            return operationVM;
        }

        public OperationInputViewModel OperationDTOToInputVM(OperationDTO model)
        {
            var operationIVM = new OperationInputViewModel();

            operationIVM.Name = model.Name;
            operationIVM.PriceValue = model.PriceValue;

            return operationIVM;
        }

        public OperationDTO OperationInputVMToDTO(OperationInputViewModel model)
        {
            var operationDTO = new OperationDTO();

            operationDTO.Name = model.Name;
            operationDTO.PriceValue = model.PriceValue;

            return operationDTO;
        }

        public CarModelViewModel CarModelDTOToVM(CarModelDTO model)
        {
            var carModelVM = new CarModelViewModel();
            var manufacturerDTO = new ManufacturerDTO();
            carModelVM.Name = model.Name;
            carModelVM.ManufacturerId = model.ManufacturerId;
            carModelVM.Manufacturer = manufacturerDTO;
            carModelVM.ManufacturerName = model.Manufacturer.Name;

            return carModelVM;
        }
        public CarModelDTO CarModelInputVMToDTO(CarModelInputViewModel model)
        {
            var carModelDTO = new CarModelDTO();
            var manufacturerDTO = new ManufacturerDTO();
            carModelDTO.Name = model.Name;
            carModelDTO.Manufacturer = manufacturerDTO;
            carModelDTO.Manufacturer.Name = model.ManufacturerName;


            return carModelDTO;
        }
        public CarModelInputViewModel CarModelDTOToInputVM(CarModelDTO model)
        {
            var carModelIVM = new CarModelInputViewModel();
            var manufacturerDTO = new ManufacturerDTO();
            carModelIVM.Name = model.Name;
            manufacturerDTO.Name = model.Manufacturer.Name;
            carModelIVM.ManufacturerName = model.Manufacturer.Name;
            return carModelIVM;
        }
        public CarViewModel CarDTOToVM(CarDTO model)
        {
            var carVM = new CarViewModel();
            carVM.Id = model.Id;
            carVM.RegistrationPlate = model.RegistrationPlate;
            carVM.VIN = model.VIN;
            carVM.CarModelId = model.CarModelId;
            carVM.CarModelName = model.CarModel.Name;
            carVM.UserId = model.UserId;

            return carVM;
        }

        public UserViewModel UserDTOToVM(UserDTO model)
        {
            var userVM = new UserViewModel();

            userVM.Id = model.Id;
            userVM.UserName = model.UserName;
            userVM.Email = model.Email;
            userVM.PhoneNumber = model.PhoneNumber;

            return userVM;
        }

        public UserDTO UserVMToDTO(UserViewModel model)
        {
            var userDTO = new UserDTO();

            userDTO.Id = model.Id;
            userDTO.UserName = model.UserName;
            userDTO.Email = model.Email;
            userDTO.PhoneNumber = model.PhoneNumber;

            return userDTO;
        }

        public UserDTO UserInputVMToDTO(UserInputViewModel model)
        {
            var userDTO = new UserDTO();

            userDTO.UserName = model.UserName;
            userDTO.LastName = model.LastName;
            userDTO.Email = model.Email;
            userDTO.PhoneNumber = model.PhoneNumber;

            return userDTO;
        }

        public AssignRoleDTO AssignRoleInputVMToDTO(AssignRoleViewModel model)
        {
            var assignRoleDTO = new AssignRoleDTO();

            assignRoleDTO.Email = model.Email;
            //assignRoleDTO.Password = model.Password;
            assignRoleDTO.Role = model.Role;

            return assignRoleDTO;
        }

        public CarDTO CarInputVMToDTO(CarInputViewModel model)
        {
            var carDTO = new CarDTO();

            carDTO.UserId = model.UserId;
            carDTO.VIN = model.VIN;
            carDTO.RegistrationPlate = model.RegistrationPlate;

            return carDTO;
        }

        public CustomerFilterDTO CustomerFilterModelToDTO(CustomerFilterModel model)
        {
            var newDTO = new CustomerFilterDTO();

            newDTO.FirstName = model.FirstName;
            newDTO.LastName = model.LastName;
            newDTO.Email = model.Email;
            newDTO.Phone = model.Phone;
            newDTO.CarId = model.CarId;
            newDTO.BeforeDate = model.BeforeDate;
            newDTO.AfterDate = model.AfterDate;

            return newDTO;
        }

        public OperationsFilterDTO OperationsFilterModelToDTO(OperationsFilterModel model)
        {
            var newDTO = new OperationsFilterDTO();

            if (model.VehicleId != 0)
            {
                newDTO.VehicleId = model.VehicleId;
            }

            newDTO.BeforeDate = model.BeforeDate;
            newDTO.AfterDate = model.AfterDate;

            return newDTO;
        }

        public VisitCurrencyViewModel VisitDTOToViewModel (VisitDTO model, string currency = "EUR", double rate = 1)
        {
            var visitCurrencyVM = new VisitCurrencyViewModel();

            visitCurrencyVM.Id = model.Id;
            visitCurrencyVM.Date = model.Date;
            visitCurrencyVM.CarId = model.CarId;

            visitCurrencyVM.Operations = model.Operations.Select(o => OperationDTOToVM(o,currency,rate)).ToList();

            visitCurrencyVM.TotalPrice = model.TotalPrice * rate;
            visitCurrencyVM.TotalPrice = Math.Round(visitCurrencyVM.TotalPrice,2);
            visitCurrencyVM.Currency = currency;

            return visitCurrencyVM;
        }
    }
}
