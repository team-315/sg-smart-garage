﻿using SmartGarage.Data.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Models
{
    public class OperationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PriceValue { get; set; }
        public string PriceCurrency { get; set; }
    }
}
