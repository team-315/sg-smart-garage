﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Models
{
    public class VisitViewModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public int CarId { get; set; }
        public double TotalPrice { get; set; }
        public List<OperationDTO> Operations { get; set; }
    }
}
