﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Models
{
    public class VisitCurrencyViewModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public int CarId { get; set; }
        public double TotalPrice { get; set; }
        public string Currency { get; set; }
        public List<OperationViewModel> Operations { get; set; }
    }
}
