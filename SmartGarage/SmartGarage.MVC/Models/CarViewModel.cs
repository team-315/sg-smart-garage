﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Models
{
    public class CarViewModel
    {
        public int Id { get; set; }
        public string RegistrationPlate { get; set; } 
        public string VIN { get; set; } 
        public int CarModelId { get; set; }
        public string CarModelName { get; set; }
        public string ManufacturerName { get; set; }
        public int UserId { get; set; }

        public string UserName { get; set; }
    }
}
