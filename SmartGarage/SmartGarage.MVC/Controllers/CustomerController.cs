﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.MVC.Mapper;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Controllers
{
    public class CustomerController : Controller
    {

        private readonly ICarService carService;
        private readonly IUserService userService;
        private readonly IVisitService visitService;
        private readonly ICurrencyConverterService currencyConverterService;
        private readonly ViewModelMapper mapper;

        public CustomerController(ICurrencyConverterService currencyConverterService, ICarService carService, IUserService userService, IVisitService visitService, ViewModelMapper mapper)
        {
            this.currencyConverterService = currencyConverterService;
            this.carService = carService;
            this.userService = userService;
            this.visitService = visitService;
            this.mapper = mapper;
        }

        // GET: CustomerController
        
        [Authorize]
        public async Task<ActionResult> Index(string carId, string beforeDate, string afterDate, string currency = "EUR")
        {
            var authenticatedUserId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

            ICollection<CarDTO> cars = new List<CarDTO>();
            try
            {
                cars = await carService.GetAllCarsForUser(int.Parse(authenticatedUserId));
            }
            catch (Exception)
            {
                return RedirectToAction("Index","Customer");
            }

            ViewData["CarId"] = new SelectList(cars, "Id", "RegistrationPlate").ToList();

            var OperationFilterDTO = new OperationsFilterDTO();

            if (carId != null && int.TryParse(carId, out int vehId))
                OperationFilterDTO.VehicleId = vehId;
            if (beforeDate != null && DateTime.TryParse(beforeDate, out DateTime beforeD))
                OperationFilterDTO.BeforeDate = beforeD;
            if (afterDate != null && DateTime.TryParse(afterDate, out DateTime afterD))
                OperationFilterDTO.AfterDate = afterD;

            var visits = await this.visitService.GetAllOperationsFiltered(int.Parse(authenticatedUserId), OperationFilterDTO);

            if (currency != "EUR" && currency != null)
            {
                var rate = currencyConverterService.GetCurrency(currency);

                if (rate.Success)
                {
                    var visitcurrencyVMs = visits.Select(v => mapper.VisitDTOToViewModel(v, currency, rate.Rates.Values.First())).ToList();

                    return View(visitcurrencyVMs);
                }
                else
                {
                    return BadRequest("Something went wrong");
                }
            }

            var visitVMs = visits.Select(v => mapper.VisitDTOToViewModel(v)).ToList();
            return View(visitVMs);
        }

        // GET: CustomerController/Details/5
        public async Task<ActionResult> Details(int id, string currency = "EUR")
        {
            var visitDTO = await this.visitService.GetVisitAsync(id);

            if (currency != "EUR" && currency != null)
            {
                var rate = currencyConverterService.GetCurrency(currency);

                if (rate.Success)
                {
                    var visitcurrencyVM = mapper.VisitDTOToViewModel(visitDTO, currency, rate.Rates.Values.First());

                    return View(visitcurrencyVM);
                }
                else
                {
                    return Redirect("~/Home/PageNotFound");
                }
            }

            var visitVM = mapper.VisitDTOToViewModel(visitDTO);

            return View(visitVM);
        }
    }
}
