﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.MVC.Mapper;
using SmartGarage.MVC.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Controllers
{
    [Authorize(Roles = "Employee")]
    public class CarController : Controller
    {
        private readonly IUserService userService;
        private readonly ICarService carService;
        private readonly ICarModelService carModelService;
        private readonly IManufacturerService manufacturerService;
        private readonly ViewModelMapper mapper;
        public CarController(IUserService userService, ICarService carService, 
                             ICarModelService carModelService, ViewModelMapper mapper, 
                             IManufacturerService manufacturerService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService)); ;
            this.carService = carService ?? throw new ArgumentNullException(nameof(carService));
            this.carModelService = carModelService ?? throw new ArgumentNullException(nameof(carModelService));
            this.manufacturerService = manufacturerService ?? throw new ArgumentNullException(nameof(manufacturerService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IActionResult> Index(int userId)
        {

            var cars = await carService.GetAllCarsForUser(userId);
            var carsViewModel = cars.Select(c => mapper.CarDTOToVM(c)).ToList();

            foreach (var item in carsViewModel)
            {
                item.ManufacturerName = (await carModelService.GetCarModelAsync(item.CarModelId))?.Manufacturer?.Name;
            }

            var users = await userService.GetAllCustomersAsync();

            ViewData["UserId"] = new SelectList(users, "Id", "Email").ToList();

            
            return View(carsViewModel);
        }
        public async Task<IActionResult> Details([FromRoute] int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var car = await carService.GetCarAsync(Convert.ToInt32(id));
            
            if (car == null)
            {
                return NotFound();
            }

            var carViewModel = mapper.CarDTOToVM(car);

            return View(carViewModel);
        }

        public async Task<ActionResult> Create()
        {
            var customers = await userService.GetAllCustomersAsync();

            ViewData["UserId"] = new SelectList(customers, "Id", "Email").ToList();

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(CarInputViewModel model)
        {
            try
            {
                var carDTO = mapper.CarInputVMToDTO(model);

                var userId = await userService.ValidateUserExist(model.UserId);

                var manufacturerId = await manufacturerService.CreateIfNotExistAsync(model.ManufacturerName);

                var carModelId = await carModelService.CreateCarModelIfNotExistAsync(model.CarModelName, manufacturerId);

                carDTO.UserId = userId;
                carDTO.CarModelId = carModelId;

                var newCar = await this.carService.CreateCarAsync(carDTO);

                return RedirectToAction(nameof(Index));
            }
            catch (ResourceAlreadyExistException e)
            {
                return Conflict(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        
        public async Task<IActionResult> Edit(int id, CarInputViewModel car)
        {
            try
            {
                var carDTO = mapper.CarInputVMToDTO(car);
                carDTO.Id = id;

                //var userId = await userService.ValidateUserExist(car.UserId);
                //carDTO.UserId = userId;

                var manufacturerId = await manufacturerService.CreateIfNotExistAsync(car.ManufacturerName);

                var carModelId = await carModelService.CreateCarModelIfNotExistAsync(car.CarModelName, manufacturerId);
                carDTO.CarModelId = carModelId;

                await this.carService.UpdateCarAsync(carDTO);

                return RedirectToAction(nameof(Index));
            }
            catch (ResourceNotExistException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
