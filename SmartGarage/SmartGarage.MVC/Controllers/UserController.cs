﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.Data.Models;
using SmartGarage.MVC.Mapper;
using SmartGarage.MVC.Models;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using SmartGarage.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Controllers
{

    [Authorize(Roles = "Employee")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IMailService mailService;
        private readonly ICarService carService;
        private readonly IVisitService visitService;
        private readonly ViewModelMapper mapper;

        public UserController(IUserService userService, IVisitService visitService, IMailService mailService, ViewModelMapper mapper, ICarService carService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.visitService = visitService;
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.carService = carService ?? throw new ArgumentNullException(nameof(carService));
            this.mailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
        }

        public async Task<IActionResult> Index(string firstName, string lastName, string carId, string email, string phone, string beforeDate, string afterDate, string order)
        {
            var cars = await carService.GetAllCarsAsync();

            ViewData["CarId"] = new SelectList(cars, "Id", "RegistrationPlate").ToList();

            if (order == null)
            {
                var filterDTO = new CustomerFilterDTO();

                if (carId != null && int.TryParse(carId, out int vehId))
                    filterDTO.CarId = vehId;
                if (beforeDate != null && DateTime.TryParse(beforeDate, out DateTime beforeD))
                    filterDTO.BeforeDate = beforeD;
                if (afterDate != null && DateTime.TryParse(afterDate, out DateTime afterD))
                    filterDTO.AfterDate = afterD;
                filterDTO.FirstName = firstName;
                filterDTO.LastName = lastName;
                filterDTO.Phone = phone;
                filterDTO.Email = email;

                var users = await userService.GetAllCustomersIncludingDeletededFilteredAsync(filterDTO);

                var usersViewModel = users.Select(u => mapper.UserDTOAdminToVM(u)).ToList();

                return View(usersViewModel);
            }
            else if (order == "name")
            {
                var users = await userService.GetAllCustomersSortedByNameAsync();

                var usersViewModel = users.Select(u => mapper.UserDTOAdminToVM(u)).ToList();

                return View(usersViewModel);
            }
            else
            {
                var userIds = await visitService.GetAllUserIdsOrderedByVisitDate();

                var users = await userService.GetAllCustomersSortedByVisitsAsync(userIds);

                var usersViewModel = users.Select(u => mapper.UserDTOAdminToVM(u)).ToList();

                return View(usersViewModel);
            }
            
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userDTO = await userService.AdminGetUserAsync(Convert.ToInt32(id));
            var userViewModel = mapper.UserDTOAdminToVM(userDTO);
            return View(userViewModel);
        }

        public async Task<IActionResult> Details([FromRoute] int id)
        {
            if (id == 0)
            {
                return NotFound();
            }

            var user = await userService.AdminGetUserAsync(Convert.ToInt32(id));

            if (user == null)
            {
                return NotFound();
            }

            var userViewModel = mapper.UserDTOAdminToVM(user);

            return View(userViewModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                await userService.DeleteAsync(Convert.ToInt32(id));
            }
            catch (ResourceNotExistException e)
            {
                return BadRequest(e.Message);
            }

            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }

            TempData["message"] = "User is deleted !";
            return RedirectToAction(nameof(Index));
        }

        [HttpPost, ActionName("UndoDelete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UndoDeleteConfirmed(int id)
        {
            try
            {
                await userService.UndoDeleteUserAsync(Convert.ToInt32(id));
            }
            catch (ResourceNotExistException e)
            {
                return BadRequest(e.Message);
            }

            catch (InvalidParameterException e)
            {
                return BadRequest(e.Message);
            }

            TempData["message"] = "User is restored !";
            return RedirectToAction(nameof(Index));
        }

        // GET: Admin/Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            return View();
        }

        //POST: Admin/Users/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, UserInputViewModel user)
        {
            try
            {
                var userDTO = mapper.UserInputVMToDTO(user);
                userDTO.Id = id;
                await userService.UpdateAsync(userDTO);
                //password?
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Create()
        {
            return Redirect("~/Identity/Account/Register");
        }

    }
}
