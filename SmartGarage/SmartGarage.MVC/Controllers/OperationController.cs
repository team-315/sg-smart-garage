﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.MVC.Mapper;
using SmartGarage.MVC.Models;
using SmartGarage.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Controllers
{
    public class OperationController : Controller
    {
        private readonly ViewModelMapper mapper;
        private readonly IOperationService operationService;

        public OperationController(IOperationService operationService, ViewModelMapper mapper)
        {
            this.operationService = operationService;
            this.mapper = mapper;
        }
        // GET: OperationController
        public async Task<ActionResult> Index(string name, int price)
        {
            var operationDTOs = await operationService.GetAllOperationsByNameAndPriceAsync(name, price);

            var operationVMs = operationDTOs.Select(o => mapper.OperationDTOToVM(o)).ToList();

            return View(operationVMs);
        }

        // GET: OperationController/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }

        // GET: OperationController/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: OperationController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(OperationInputViewModel model)
        {
            try
            {
                var dto = mapper.OperationInputVMToDTO(model);

                await operationService.CreateAsync(dto);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OperationController/Edit/5
        public ActionResult Update(int id)
        {
            return View();
        }

        // POST: OperationController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(int id, OperationInputViewModel model)
        {
            try
            {
                var operationDTO = mapper.OperationInputVMToDTO(model);
                operationDTO.Id = id;

                await operationService.UpdateAsync(operationDTO);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: OperationController/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await operationService.DeleteAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
