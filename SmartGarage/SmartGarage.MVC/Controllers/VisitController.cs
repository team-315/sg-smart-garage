﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.MVC.Mapper;
using SmartGarage.Services.Contracts;
using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.MVC.Controllers
{
    public class VisitController : Controller
    {
        private readonly ICarService carService;
        private readonly IUserService userService;
        private readonly IVisitService visitService;
        private readonly IOperationService operationService;
        
        private readonly ViewModelMapper mapper;

        public VisitController(IOperationService operationService, ICarService carService, IUserService userService, IVisitService visitService, ViewModelMapper mapper)
        {
            this.operationService = operationService;
            this.carService = carService;
            this.userService = userService;
            this.visitService = visitService;
            this.mapper = mapper;
        }

        // GET: VisitController
        public async Task<ActionResult> Index(int id, string carId, string beforeDate, string afterDate)
        {
            var cars = await carService.GetAllCarsForUser(id);

            ViewData["CarId"] = new SelectList(cars, "Id", "RegistrationPlate").ToList();

            var OperationFilterDTO = new OperationsFilterDTO();

            if (carId != null && int.TryParse(carId, out int vehId))
                OperationFilterDTO.VehicleId = vehId;
            if (beforeDate != null && DateTime.TryParse(beforeDate, out DateTime beforeD))
                OperationFilterDTO.BeforeDate = beforeD;
            if (afterDate != null && DateTime.TryParse(afterDate, out DateTime afterD))
                OperationFilterDTO.AfterDate = afterD;

            var visits = await this.visitService.GetAllOperationsFiltered(id, OperationFilterDTO);

            var visitVMs = visits.Select(v => mapper.VisitDTOToViewModel(v)).ToList();
            return View(visitVMs);
        }
        
        // GET: VisitController/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var visitDTO = await this.visitService.GetVisitAsync(id);

            var visitVM = mapper.VisitDTOToViewModel(visitDTO);

            return View(visitVM);
        }

        // GET: VisitController/Create
        public async Task<ActionResult> Create(int id)
        {
            var cars = await carService.GetAllCarsForUser(id);

            ViewData["CarId"] = new SelectList(cars, "Id", "RegistrationPlate").ToList();

            var ops = await operationService.GetAllOperationsAsync();

            ViewData["OperationId"] = new SelectList(ops, "Id", "Name").ToList();

            return View();
        }

        // POST: VisitController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int carId, List<int> operationId)
        {
            
            try
            {
                await visitService.CreateAsync(carId, operationId);

                return RedirectToAction("Index","User");
            }
            catch
            {
                return View();
            }
        }
    }
}
