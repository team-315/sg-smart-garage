﻿using SmartGarage.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using SmartGarage.MVC.Models;
using System.Linq;

namespace SmartGarage.MVC.Mapper
{
    public class ViewModelMapper
    {
        public VisitViewModel VisitDTOToVM(VisitDTO model)
        {
            var visitVM = new VisitViewModel();

            visitVM.Id = model.Id;
            visitVM.CarId = model.CarId;
            visitVM.Date = model.Date;
            visitVM.Operations = model.Operations;
            visitVM.TotalPrice = model.TotalPrice;

            return visitVM;
        }

        public OperationsFilterDTO OperationFilterVMToDTO(OperationFilterViewModel model)
        {
            var vm = new OperationsFilterDTO();

            vm.AfterDate = model.AfterDate;
            vm.BeforeDate = model.BeforeDate;
            vm.VehicleId = model.VehicleId;

            return vm;
        }

        public VisitCurrencyViewModel VisitDTOToViewModel(VisitDTO model, string currency = "EUR", double rate = 1)
        {
            var visitCurrencyVM = new VisitCurrencyViewModel();

            visitCurrencyVM.Id = model.Id;
            visitCurrencyVM.Date = model.Date;
            visitCurrencyVM.CarId = model.CarId;

            visitCurrencyVM.Operations = model.Operations.Select(o => OperationDTOToVM(o, currency, rate)).ToList();

            visitCurrencyVM.TotalPrice = model.TotalPrice * rate;
            visitCurrencyVM.TotalPrice = Math.Round(visitCurrencyVM.TotalPrice, 2);
            visitCurrencyVM.Currency = currency;

            return visitCurrencyVM;
        }

        public OperationViewModel OperationDTOToVM(OperationDTO model, string currency = "EUR", double rate = 1)
        {
            var operationVM = new OperationViewModel();

            operationVM.Id = model.Id;
            operationVM.Name = model.Name;
            operationVM.PriceValue = model.PriceValue * rate;
            operationVM.PriceValue = Math.Round(operationVM.PriceValue, 2);
            operationVM.PriceCurrency = currency;

            return operationVM;
        }

        public OperationDTO OperationInputVMToDTO(OperationInputViewModel model)
        {
            var operationDTO = new OperationDTO();

            operationDTO.Name = model.Name;
            operationDTO.PriceValue = model.PriceValue;

            return operationDTO;
        }
        
        public CarViewModel CarDTOToVM(CarDTO model)
        {
            var carVM = new CarViewModel();
            carVM.Id = model.Id;
            carVM.RegistrationPlate = model.RegistrationPlate;
            carVM.VIN = model.VIN;
            carVM.CarModelId = model.CarModelId;
            carVM.CarModelName = model.CarModel.Name;
            carVM.UserName = model.User.UserName+" "+model.User.LastName;
            carVM.ManufacturerName = model.CarModel.Manufacturer.Name;
            return carVM;
        }

        public UserViewModelAdmin UserDTOAdminToVM(UserDTOAdmin model)
        {
            var userVM = new UserViewModelAdmin();

            userVM.Id = model.Id;
            userVM.UserName = model.UserName;
            userVM.LastName = model.LastName;
            userVM.Email = model.Email;
            userVM.PhoneNumber = model.PhoneNumber;
            userVM.IsDeleted = model.IsDeleted;
            return userVM;
        }

        public UserDTO UserInputVMToDTO(UserInputViewModel model)
        {
            var userDTO = new UserDTO();

            userDTO.UserName = model.UserName;
            userDTO.LastName = model.LastName;
            userDTO.Email = model.Email;
            userDTO.PhoneNumber = model.PhoneNumber;

            return userDTO;
        }


        public CarDTO CarInputVMToDTO(CarInputViewModel model)
        {
            var carDTO = new CarDTO();

            carDTO.UserId = model.UserId;
            carDTO.VIN = model.VIN;
            carDTO.RegistrationPlate = model.RegistrationPlate;

            return carDTO;
        }

        
    }
}
