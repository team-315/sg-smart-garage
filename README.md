# Smart Garage
A web application that enables the owners of an auto repair shop to manage their day-to-day job. 

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/Homepage.png)

## Team Members
* Rosen Bozhkov - [GitLab](https://gitlab.com/Rbozhkov77)
* Nikolay Lyutskanov - [GitLab](https://gitlab.com/lyutskanovn)

[**Click Here**](https://trello.com/b/CVjvuOa2/smart-garage) for our Trello board

## Project Description
### Areas
* **Public part** -  accessible without authentication
* **Private part** - available for registered users only
* **Administrative part** - available for administrators only

#### Public Part
* The public part is visible for users without authentication. If they want to be able to see the operations on their cars, they should log in or register.

    * LogIn page:

     ![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/LogIn.png)

    * Register page:

     ![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/Register.png)

#### Private Part - Users

* After login, users see what is visible to website visitors and additionally they can:
    
     * See all services linked to them and filter by Vechicle, Date and change Currency

     ![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CustomerView.png)
     
     * See a detailed report of the visit
     
     ![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CustomerVisitReport.png)
     
#### Private Part - Employees
* Employees have a bit more capabilities, they can:
     * Manage customers – CRUD operations, Filter, Sort By, Get Visits
     * Manage cars – Get, Create, Update, Filtrations
     * Manage services - CRUD operations, Filtrations
     
* Employee page

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/EmployeeView.png)

* See all customer, filter them by Name, Email, Phone and Date

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/FilterCustomers.png)

* See customer Details and Edit or Delete customer

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CustomerDetails.png)

* Have the oportunity to restore a customer profile after deletion

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/RestoreDeleted.png)

* See customer's visits, view each visit details and filter by date and/or vechicle

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CustomerVisits.png)

* Add new Visit

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/AddVisit.png)

* See all available Services, have CRUD operations for them, Filter them by Name, Price

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/AllServices.png)

* Edit or Create a Service

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CreateService.png)

* See all vechicles, have Create, Edit and Filter by Customer

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CarsView.png)

* Create, Edit vechicle

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/AddCar.png)

* View car Details

![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/CarDetails.png)


## Technologies
* ASP.NET Core
* ASP.NET Identity
* Entity Framework Core
* MS SQL Server
* HTML
* CSS
* Bootstrap

## Notes
* In-memory database, MSTest and Moq for testing
* Data transfer objects(DTOs)
* Above 80% Unit test code coverage of the business logic

## Database Diagram
![Alt text](https://gitlab.com/team-315/sg-smart-garage/-/raw/master/Images/DatabaseDiagram.png)
